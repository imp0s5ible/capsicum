import std.stdio;
import std.range;
import std.range.primitives;
import std.traits;
import std.array;

import capsicum.Project;

version (dlangui) {
    import dlangui;
    import capsicum.utils.Indexer;

    mixin APP_ENTRY_POINT;

    /// Entry point for dlangui based application
    extern (C) int UIAppMain(string[] args) {
        import capsicum.adapters.CellAdapter;

        Window window = Platform.instance.createWindow("DlangUI example - HelloWorld", null);

        auto layout = new TableLayout();
        layout.colCount = 2;
        layout.margins = 20; // distance from window frame to vlayout background
        layout.padding = 10; // distance from vlayout background bound to child widgets
        layout.backgroundColor = 0xFFFFC0; // yellow background color

        auto grid = Grid();
        with (grid) {
        }
        layout.addChild(grid);

        // create some widget to show in window
        window.mainWidget = layout;

        window.show();

        Log.d("TEST");
        return Platform.instance.enterMessageLoop();
    }

} else version (gtkd) {
    import gtk.MainWindow;
    import gtk.Main;

    import std.typecons : Tuple;

    import capsicum.adapters.ProjectWindow;
    import capsicum.utils.Indexer;

    void main(string[] args)
    {
        Main.init(args);

        auto project = new Project();
        auto win = new ProjectWindow(project);

        Main.run();
    }
} else version (unittest) {

} else {
    void main()
    {
        string[string] dict = [ "pepper" : "jalapeno" ];
        writeln("Hello, spicy ", dict["pepper"], "s!");
    }
}


