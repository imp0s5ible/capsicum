module capsicum.utils.Indexer;

import std.json;
import std.typecons : Flag, Yes, No;

debug import std.stdio;

struct Slice(Index)
{
    public
    {
        Index begin;
        Index end;
        pure this(Index b, Index e)
        {
            begin = b;
            end = e;
        }

        pure this(OtherIndex)(const ref Slice!OtherIndex otherSlice) inout
        {
            this.begin = cast(Index) otherSlice.begin;
            this.end = cast(Index) otherSlice.end;
        }

        inout auto opAdd(OtherIndex)(const ref Slice!OtherIndex otherSlice)
        in
        {
            assert(begin + otherSlice.end <= end);
        }
        body
        {
            return Slice!Index(begin + otherSlice.begin, begin + otherSlice.end);
        }
    }

    public pure @property auto length() const
    {
        return end - begin;
    }

    public pure @property ref auto length(SizeT)(SizeT newLength)
    in
    {
        assert(newLength > 0);
    }
    body
    {
        this.end = begin + newLength;
        return this;
    }
}

/** Adds two dimensional indexing support to any random access range.
 * Supports compile-time and run-time row widths. Instantiating the indexer with a static row width
 * of zero creates an indexer with dynamic row width. */
public struct TwoDimensionalIndexer(size_t staticRowWidth = 0, Range)
{
    import std.typecons;

    private
    {
        import std.range.primitives;
        import std.traits;

        Range rangeBehind;

        alias CoordinateType = Tuple!(size_t, size_t);

        alias Elem = ForeachType!Range;

        Slice!size_t rowSlice;
        Slice!size_t columnSlice;
        size_t numberOfRows;

        static if (staticRowWidth == 0)
        {
            size_t numberOfColumns;

            /** Slice constructor */
            pure this(inout Range range, const size_t newRowWidth,
                    const Slice!size_t newRowSlice, const Slice!size_t newColumnSlice) inout
            {
                rangeBehind = range;
                numberOfColumns = newRowWidth;
                numberOfRows = rangeBehind.length / numberOfColumns;
                rowSlice = newRowSlice;
                columnSlice = newColumnSlice;
            }
        }
        else
        {
            alias numberOfColumns = staticRowWidth;
        }

        inout auto Translate2DIndex(RowIndex, ColumnIndex)(RowIndex rowIndex,
                ColumnIndex columnIndex)
        in
        {
            assert(IsValidIndex(rowIndex, columnIndex));
        }
        out (result)
        {
            import capsicum.utils.Range;

            assert(rangeBehind.IsValidIndex(result));
        }
        body
        {
            import std.stdio;

            immutable auto flatIndex = (numberOfColumns * rowIndex) + columnIndex;
            return flatIndex;
        }

    }

    public
    {
        static if (staticRowWidth == 0)
        {
            /** Dynamic row width constructor */
            pure this(inout Range range, const size_t newRowWidth) inout
            {
                rangeBehind = range;
                numberOfColumns = newRowWidth;
                numberOfRows = rangeBehind.length / numberOfColumns;
                rowSlice = Slice!size_t(cast(size_t) 0u, numberOfRows);
                columnSlice = Slice!size_t(cast(size_t) 0u, numberOfColumns);
            }

            immutable(TwoDimensionalIndexer!(staticRowWidth, Range)) idup() const
            {
                return immutable(TwoDimensionalIndexer!(staticRowWidth, Range))(rangeBehind.idup,
                        numberOfColumns, rowSlice, columnSlice);
            }
        }
        else
        {
            /** Static row width constructor */
            pure this(inout Range range) inout
            {
                rangeBehind = range;
                numberOfRows = rangeBehind.length / numberOfColumns;
                rowSlice = Slice!size_t(cast(size_t) 0u, numberOfRows);
                columnSlice = Slice!size_t(cast(size_t) 0u, numberOfColumns);
            }

            immutable(TwoDimensionalIndexer!(staticRowWidth, Range)) idup() const
            {
                return immutable(TwoDimensionalIndexer!(staticRowWidth, Range))(rangeBehind.idup);
            }
        }

        pure auto @property maxWidth() const
        {
            return numberOfColumns;
        }

        pure auto @property maxHeight() const
        {
            return numberOfRows;
        }

        pure auto @property rows() const
        {
            import std.range;

            static if (staticRowWidth == 0u)
            {
                import std.algorithm;

                return rangeBehind.chunks(numberOfColumns)[rowSlice.begin .. rowSlice.end].map!(
                        row => row[columnSlice.begin .. columnSlice.end]);
            }
            else
            {
                return rangeBehind.chunks(numberOfColumns);
            }
        }

        pure auto @property rows()
        {
            import std.range;

            static if (staticRowWidth == 0u)
            {
                import std.algorithm;

                return rangeBehind.chunks(numberOfColumns)[rowSlice.begin .. rowSlice.end].map!(
                        row => row[columnSlice.begin .. columnSlice.end]);
            }
            else
            {
                return rangeBehind.chunks(numberOfColumns);
            }
        }

        pure @property size_t width() const
        {
            return columnSlice.length;
        }

        import std.algorithm.comparison : min;

        pure @property auto ref width(size_t newWidth)
        {
            columnSlice.length = min(newWidth, numberOfColumns - columnSlice.begin);
            return this;
        }

        pure @property size_t height() const
        {
            return rowSlice.length;
        }

        pure @property auto ref height(size_t newHeight)
        {
            rowSlice.length = min(newHeight, numberOfRows - rowSlice.begin);
            return this;
        }

        auto pure opIndex(RowIndex, ColumnIndex)(const Slice!RowIndex inRowSlice,
                const Slice!ColumnIndex inColumnSlice) inout
        {
            Slice!size_t nRowSlice = rowSlice + inRowSlice;
            Slice!size_t nColumnSlice = columnSlice + inColumnSlice;
            inout auto slicedIndexer = inout(.TwoDimensionalIndexer!(0u, Range))(rangeBehind,
                    numberOfColumns, nRowSlice, nColumnSlice);
            return slicedIndexer;
        }

        inout auto opIndex(RowIndex, ColumnIndex)(const RowIndex rowIndex,
                const ColumnIndex columnIndex)
                if (!hasMember!(RowIndex, "begin") && !hasMember!(ColumnIndex, "begin"))
        in
        {
            assert(IsValidIndex(rowIndex, columnIndex));
        }
        body
        {
            return rows[rowIndex][columnIndex];
        }

        inout auto opIndex(RowIndex, ColumnIndex)(const Tuple!(RowIndex, ColumnIndex) indexTuple)
                if (!hasMember!(RowIndex, "begin") && !hasMember!(ColumnIndex, "begin"))
        {
            return opIndex!(RowIndex, ColumnIndex)(indexTuple[0], indexTuple[1]);
        }

        inout auto opSlice(int dimension)(size_t begin, size_t end)
                if (0 <= dimension && dimension < 2)
        {
            return Slice!size_t(begin, end);
        }

        int opApply(int delegate(ref Elem) loopBody)
        {
            foreach (ref row; rows)
            {
                foreach (ref val; row)
                {
                    if (!loopBody(val))
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }

        auto opIndexAssign(Value, RowIndex, ColumnIndex)(const Value value,
                const RowIndex rowIndex, const ColumnIndex columnIndex)
        in
        {
            import std.stdio;

            assert(IsValidIndex(rowIndex, columnIndex));
        }
        body
        {
            return rows[rowIndex][columnIndex] = value;
        }

        auto opIndexAssign(Value, RowIndex, ColumnIndex)(const Value value,
                const Tuple!(RowIndex, ColumnIndex) indexTuple)
        {
            return opIndexAssign(value, indexTuple[0], indexTuple[1]);
        }

        size_t opDollar(size_t pos)() const
        {
            static assert(0 <= pos && pos < 2, "Only dimensions 0 and 1 are supported!");
            static if (pos == 0)
            {
                return height;
            }
            else static if (pos == 1)
            {
                return width;
            }
        }

        static if (is(typeof(rangeBehind.front)))
        {
            auto front() const
            {
                return rangeBehind.front;
            }
        }

        auto ValuesOf() const
        {
            import capsicum.utils.Range;

            return rangeBehind.ValuesOf();
        }

        auto IndicesOf() const
        {
            import std.algorithm.setops;
            import std.range;

            auto ret = cartesianProduct(iota(0, opDollar!0), iota(0, opDollar!1));
            return ret;
        }

        bool IsValidIndex(RowIndex, ColumnIndex)(RowIndex rowIndex, ColumnIndex columnIndex) const
        {
            import capsicum.utils.Range;

            immutable bool result = 0 <= rowIndex && rowIndex < height
                && 0 <= columnIndex && columnIndex < width;
            return result;
        }

        bool IsValidIndex(IndexTuple)(IndexTuple indexTuple) const
        {
            return IsValidIndex(indexTuple[0], indexTuple[1]);
        }

        @property auto length() const
        {
            return rangeBehind.length;
        }

        JSONValue opCast(T : JSONValue)() const
        {
            import std.algorithm.iteration : map;
            import std.array : array;
            import std.conv : to;
            import capsicum.utils.Range;

            JSONValue result = ["rowWidth" : width];
            result.object["values"] = JSONValue(new JSONValue[0]);
            foreach (const row; rows)
            {
                result.object["values"] ~= row.map!(to!JSONValue).array;
            }
            return result;
        }
    }
}

auto twoDimensionalIndexer(Range)(Range range, size_t rowWidth)
{
    return TwoDimensionalIndexer!(0, Range)(range, rowWidth);
}

auto twoDimensionalIndexer(size_t staticRowWidth, Range)(Range range)
{
    return TwoDimensionalIndexer!(staticRowWidth, Range)(range);
}

auto twoDimensionalArrayFromJson(Elem)(const JSONValue jsonValue,
        const size_t allocateWidth = 4u, const size_t allocateHeight = 4u)
{
    import std.array;
    import std.algorithm.iteration : map;
    import std.algorithm.comparison : max;
    import std.conv : to;
    import capsicum.utils.Range : CopyByIndex;

    auto jsonValues = jsonValue["values"].array.map!(Elem.createFromJson).array;
    const size_t jsonWidth = cast(size_t) jsonValue["rowWidth"].integer;
    const size_t jsonHeight = jsonValues.length / jsonWidth;

    const size_t actualWidth = max(allocateWidth, jsonWidth);
    const size_t actualHeight = max(allocateHeight, jsonHeight);

    Elem[] buffer = new Elem[actualWidth * actualHeight];
    auto source = TwoDimensionalIndexer!(0, Elem[])(jsonValues, jsonWidth);

    auto result = TwoDimensionalIndexer!(0, Elem[])(buffer, actualWidth);
    source.CopyByIndex(result);

    return result[0 .. jsonHeight, 0 .. jsonWidth];
}

version (unittest)
{
    import capsicum.utils.Unittest;

    immutable int[] array = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    unittest
    {
        mixin(unittestDataString("TwoDimensionalIndexer", "Sanity check for static row width"));
        immutable auto indexer = twoDimensionalIndexer!3(array);

        assert(indexer.width == 3);
        assert(indexer.height == 3);

        assert(indexer.IsValidIndex(0, 0));
        assert(indexer[0, 0] == 1);
        assert(indexer.IsValidIndex(0, 1));
        assert(indexer[0, 1] == 2);
        assert(indexer.IsValidIndex(0, 2));
        assert(indexer[0, 2] == 3);
        assert(indexer.IsValidIndex(1, 0));
        assert(indexer[1, 0] == 4);
        assert(indexer.IsValidIndex(1, 1));
        assert(indexer[1, 1] == 5);
        assert(indexer.IsValidIndex(1, 2));
        assert(indexer[1, 2] == 6);
        assert(indexer.IsValidIndex(2, 0));
        assert(indexer[2, 0] == 7);
        assert(indexer.IsValidIndex(2, 1));
        assert(indexer[2, 1] == 8);
        assert(indexer.IsValidIndex(2, 2));
        assert(indexer[2, 2] == 9);

    }

    unittest
    {
        mixin(unittestDataString("TwoDimensionalIndexer", "Slice test for static row width"));

        immutable auto indexer = twoDimensionalIndexer!3(array);

        immutable auto sliceLowerRight = indexer[1 .. 3, 1 .. 3];
        assert(sliceLowerRight.width == 2);
        assert(sliceLowerRight.height == 2);
        assert(sliceLowerRight[0, 0] == 5);
        assert(sliceLowerRight[0, 1] == 6);
        assert(sliceLowerRight[1, 0] == 8);
        assert(sliceLowerRight[1, 1] == 9);

        immutable auto sliceUpperRight = indexer[0 .. 2, 1 .. 3];
        assert(sliceUpperRight.width == 2);
        assert(sliceUpperRight.height == 2);
        assert(sliceUpperRight[0, 0] == 2);
        assert(sliceUpperRight[0, 1] == 3);
        assert(sliceUpperRight[1, 0] == 5);
        assert(sliceUpperRight[1, 1] == 6);

        immutable auto sliceUpperLeft = indexer[0 .. 2, 0 .. 2];
        assert(sliceUpperLeft.width == 2);
        assert(sliceUpperLeft.height == 2);
        assert(sliceUpperLeft[0, 0] == 1);
        assert(sliceUpperLeft[0, 1] == 2);
        assert(sliceUpperLeft[1, 0] == 4);
        assert(sliceUpperLeft[1, 1] == 5);

        immutable auto sliceLowerLeft = indexer[1 .. 3, 0 .. 2];
        assert(sliceLowerLeft.width == 2);
        assert(sliceLowerLeft.height == 2);
        assert(sliceLowerLeft[0, 0] == 4);
        assert(sliceLowerLeft[0, 1] == 5);
        assert(sliceLowerLeft[1, 0] == 7);
        assert(sliceLowerLeft[1, 1] == 8);
    }

    unittest
    {
        mixin(unittestDataString("TwoDimensionalIndexer", "Sanity check for dynamic row width"));

        immutable auto indexer = twoDimensionalIndexer(array, 3);

        assert(indexer.width == 3);
        assert(indexer.height == 3);

        assert(indexer.IsValidIndex(0, 0));
        assert(indexer[0, 0] == 1);
        assert(indexer.IsValidIndex(0, 1));
        assert(indexer[0, 1] == 2);
        assert(indexer.IsValidIndex(0, 2));
        assert(indexer[0, 2] == 3);
        assert(indexer.IsValidIndex(1, 0));
        assert(indexer[1, 0] == 4);
        assert(indexer.IsValidIndex(1, 1));
        assert(indexer[1, 1] == 5);
        assert(indexer.IsValidIndex(1, 2));
        assert(indexer[1, 2] == 6);
        assert(indexer.IsValidIndex(2, 0));
        assert(indexer[2, 0] == 7);
        assert(indexer.IsValidIndex(2, 1));
        assert(indexer[2, 1] == 8);
        assert(indexer.IsValidIndex(2, 2));
        assert(indexer[2, 2] == 9);
    }
}
