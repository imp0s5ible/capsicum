module capsicum.utils.Enum;

template NameValuePairs(EnumType) if (is(EnumType == enum))
{
    auto NameValuePairs()
    {
        import std.conv : to;
        import std.range : zip;
        import std.array : array, assocArray;
        import std.algorithm.iteration : map;
        import std.traits : EnumMembers;

        auto strings = [__traits(allMembers, EnumType)];
        return zip(strings, [EnumMembers!EnumType]).assocArray;
    }
}
