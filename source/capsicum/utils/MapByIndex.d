module capsicum.utils.MapByIndex;

private
{
    import std.traits;
    import std.range.primitives;

    struct MapByIndexResult(alias Function, Range)
    {
        import capsicum.utils.Range;

        private
        {
            Range rangeToIterate;
            alias RetType = ReturnType!Function;
        }

        public
        {
            this(Range range)
            {
                rangeToIterate = range;
            }

            static if (isInputRange!Range)
            {
                auto front()
                {
                    import std.stdio;

                    return Function(rangeToIterate.front());
                }

                void popFront()
                {
                    return rangeToIterate.popFront();
                }

                bool empty()
                {
                    return rangeToIterate.empty();
                }
            }

            auto opIndex(Indices...)(Indices indices) inout /* if (is(typeof(rangeToIterate.opIndex(Indices)))) */
            {
                import std.stdio;

                return Function(rangeToIterate[indices]);
            }

            int opApply(Index)(int delegate(RetType) loopBody)
            {
                foreach (const ref Index ind, ref Value val; rangeToIterate)
                {
                    loopBody(ind, Function(val));
                }
            }

            int opApply(Index)(int delegate(Index, RetType) loopBody)
            {
                foreach (const ref Index ind, ref Value val; rangeToIterate)
                {
                    loopBody(ind, Function(val));
                }
            }

            bool SupportsAllIndicesOf(Range)(const ref Range range)
            {
                return rangeToIterate.SupportsAllIndicesOf(range);
            }

            const pure auto IndicesOf()
            {
                return rangeToIterate.IndicesOf;
            }

            const pure bool IsValidIndex(Index)(const ref Index index)
            {
                return rangeToIterate.IsValidIndex(index);
            }
        }
    }
}

public
{
    auto MapByIndex(alias Function, Range)(Range range)
    {
        return MapByIndexResult!(Function, Range)(range);
    }
}
