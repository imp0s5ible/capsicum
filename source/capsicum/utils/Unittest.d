module capsicum.utils.Unittest;

shared uint gTestIndex = 0;

immutable(string) unittestDataString(immutable string testName, immutable string testDescription = "")
{
    assert(__ctfe, "This function is CTFE only!");

    return "immutable testName = \"" ~ testName
        ~ "\";\nimmutable testDescription = \"" ~ testDescription ~ "\";\n" ~ q{
    import std.stdio;
	import std.outbuffer;


    uint thisTestIndex;
    synchronized {
        import core.atomic;
        thisTestIndex = (atomicOp!"+="(gTestIndex, 1));
        writeln ("[TEST #", thisTestIndex, "] - ", testName, ":\n         " , testDescription);
    }
	OutBuffer debugLog = new OutBuffer();
    scope(success) writeln("[TEST #", thisTestIndex, " PASSED]\n");
	scope(failure)  {
		writeln("[TEST #", thisTestIndex, " FAILED] - Log:\n");
		writeln(debugLog.toString());
	}
    };
}
