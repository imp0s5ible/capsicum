module capsicum.utils.Metaprogramming;

template InstantiateIfTemplate(alias Tmp)
{
    static if (__traits(isTemplate, Tmp))
    {
        alias InstantiateIfTemplate = Tmp!();
    }
    else
    {
        alias InstantiateIfTemplate = Tmp;
    }
}
