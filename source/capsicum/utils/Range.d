module capsicum.utils.Range;

/*
bool IsValidIndex(RangeType, IndexType)(const ref IndexType index, const ref RangeType range) @safe {
	try {
		const auto elem = range[index];
	} catch (Exception e) {
		return false;
	}
	return true;
}
*/

bool IsValidIndex(RangeType : T[], IndexType, T)(inout ref RangeType range, const ref IndexType index) @safe
{
    import std.traits;

    return (0 <= index) && (index < cast(IndexType) range.length);
}

bool IsValidIndex(RangeType : T[I], IndexType, T, I)(inout ref RangeType range,
        const ref IndexType index) @safe
{
    return (cast(I) index in range) != null;
}

auto IndicesOf(RangeType : T[], T)(const ref RangeType range) @safe
{
    import std.range;

    return iota(0, range.length);
}

auto IndicesOf(RangeType : T[I], T, I)(const ref RangeType range) @trusted
{
    return range.byKey;
}

bool NoSharedElements(R, T)(R lhs, T rhs) @safe
{
    return setIntersection(lhs, rhs).length == 0;
}

auto LargestIndexOf(RangeTypes...)(const ref RangeTypes ranges) @safe
{
    import std.algorithm.comparison;

    return max(ranges[0].LargestIndexOf, ranges[1 .. $].LargestIndexOf);
}

auto LargestIndexOf(RangeType)(const ref RangeType range) @trusted
{
    import std.algorithm.searching;

    return range.IndicesOf.maxPos.front;
}

auto LargestIndexOf(RangeType : T[], T)(const ref RangeType range) @safe
{
    return range.length - 1;
}

ref ToRange CopyByIndex(FromRange, ToRange)(const FromRange from, ref ToRange to)
{
    debug import std.format : format;
    foreach (const ref index; from.IndicesOf)
    {
        debug assert(to.IsValidIndex(index), format("Out of the indices %s:\n%s is not a valid index of the destination range:\n%s!", from.IndicesOf, index, to));
        to[index] = from[index];
    }
    return to;
}

bool SupportsAllIndicesOf(Range, OtherRange)(const ref Range lhs, const ref OtherRange rhs)
{
    foreach (const ref index; rhs.IndicesOf)
    {
        if (!lhs.IsValidIndex(index))
        {
            import std.stdio;

            return false;
        }
    }
    return true;
}

bool SupportsAllIndicesOf(Range, OtherRanges...)(const ref Range lhs,
        const ref OtherRanges otherRanges) if (otherRanges.length > 1)
{
    return lhs.SupportsAllIndicesOf(otherRanges[0]) && lhs.SupportsAllIndicesOf(otherRanges[1 .. $]);
}

RangeType CreateRangeSupportingTheIndicesOf(RangeType : T[], T, OtherRangeTypes...)(
        const ref OtherRangeTypes otherRanges)
out (result)
{
    assert(result.SupportsAllIndicesOf(otherRanges));
}
body
{
    return new T[LargestIndexOf(otherRanges) + 1];
}

RangeType CreateRangeSupportingTheIndicesOf(RangeType : T[I], T, I, OtherRangeTypes...)(
        const ref OtherRangeTypes otherRanges)
body
{
    return [];
}

ref OutRange CopyByIndexFrom(InRange, OutRange)(return ref OutRange outRange, InRange inRange)
in
{
    assert(outRange.SupportsAllIndicesOf(inRange));
}
body
{
    foreach (const ref index; inRange.IndicesOf)
    {
        import std.stdio;

        outRange[index] = inRange[index];
    }
    return outRange;
}

private struct FilterByIndexResult(alias Pred, Range) {
    private {
        Range rangeBehind;
    }

    public {
        auto opDispatch(string name, T...)(T args) {
            mixin("return rangeBehind."~name~"(args);");
        }

        auto IndicesOf() const {
            import std.algorithm.iteration : filter;
            return rangeBehind.IndicesOf.filter!(i => Pred(rangeBehind.opIndex!(typeof(i))(i)));
        }

        auto IsValidIndex(I)(I index) const {
            return rangeBehind.IsValidIndex(index) && Pred(rangeBehind.opIndex!I(i));
        }

        auto opIndex(Indices...)(Indices indices) {
            return rangeBehind[indices];
        }

        this(Range range) {
            rangeBehind = range;
        }
    }
}

auto FilterByIndex(alias Pred, Range)(Range range) {
    return FilterByIndexResult!(Pred, Range)(range);
}

inout(Range) ValuesOf(Range : T[], T)(inout Range r) {
    return r;
}

import std.traits : Unqual, hasMember;
auto ValuesOf(Range)(inout Range r) if (hasMember!(Unqual!Range, "byValue"))
{
    return r.byValue();
}

import std.traits : hasMember;
auto ValuesOf(Range)(Range range) if (hasMember!(Unqual!Range, "ValuesOf")) {
    static assert (!is(typeof(range.ValuesOf) == void), "Return type of ValuesOf cannot be void!");
    return range.ValuesOf;
}

