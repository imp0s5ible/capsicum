module capsicum.Project;

debug import std.stdio : write, writeln;

debug import std.format : format;

import std.json;
import std.traits : EnumMembers;
import std.range : cycle, take, retro, iota, generate;
import std.array : array;

import capsicum.patternMatching.RuleSet;
import capsicum.patternMatching.ruleTypes.Literal;
import capsicum.patternMatching.ruleTypes.Unbound;
import capsicum.patternMatching.RuleSetting;
import capsicum.utils.Indexer;
import capsicum.utils.Range;

struct CellType
{
    import std.typecons : Tuple;

    public
    {
        int type;
        alias ColorType = Tuple!(double, "r", double, "g", double, "b");
    }

    this(int t)
    {
        type = t;
    }

    ref CellType opAssign(T : int)(const ref int t)
    {
        type = t;
        return *this;
    }

    public ColorType getDrawColor() const
    {
        return colorTable.get(cast(NamedCellType) type, ColorType(0.0, 0.0, 0.0));
    }

    public JSONValue opCast(T : JSONValue)() const
    {
        return JSONValue(type);
    }

    public CellType opCast(T : CellType)() const
    {
        return this;
    }

    public T opCast(T)() const
    {
        return cast(T) type;
    }

    public static CellType createFromJson(JSONValue val)
    {
        return CellType(cast(int) val.integer);
    }

    private static immutable ColorType[NamedCellType] colorTable;

    static this()
    {
        colorTable = [NamedCellType.DontCare : ColorType(0.6, 0.6, 0.6),
            NamedCellType.Passable : ColorType(.8, .8, .9), NamedCellType.Blocking
            : ColorType(0.6, 0.5, 0.5), NamedCellType.Potentially_Passable
            : ColorType(.6, .6, .9), NamedCellType.Player : ColorType(.8, 0.0, 0.0)];
    }
}

enum NamedCellType : CellType
{
    DontCare = CellType(0),
    Potentially_Passable = CellType(1),
    Passable = CellType(2),
    Blocking = CellType(3),
    Player = CellType(4)
}

class Project
{
    import capsicum.patternMatching.MatchSettings : MatchSettings;

    private
    {
        enum CellType[] alteratingGrid = [NamedCellType.Passable, NamedCellType.Blocking];
        enum CellType[] completeGrid = [EnumMembers!NamedCellType];
        auto randomRuleElementPool = completeGrid;

        enum size_t mapSize = 32u;
        enum size_t ruleMaxWidth = 5u;
        enum size_t ruleMaxHeight = 5u;
        enum size_t randomRuleMaxWidth = 3u;
        static assert(randomRuleMaxWidth <= ruleMaxWidth);
        enum size_t randomRuleMaxHeight = 3u;
        static assert(randomRuleMaxHeight <= ruleMaxHeight);
        enum size_t cellSize = 16u;
        enum size_t randomRuleCount = 30u;

        enum string mapJsonFileName = "map.json";
        enum string ruleListJsonFileName = "rules.json";
        enum string persistenceFolderName = "persistence";

        auto ruleLeftSide = alteratingGrid.cycle.take(ruleMaxWidth * ruleMaxHeight).array;
        auto ruleRightSide = alteratingGrid.retro.cycle.take(ruleMaxWidth * ruleMaxHeight).array;
        auto prototypeRule = completeGrid.cycle.take(ruleMaxWidth * ruleMaxHeight).array;

        immutable CellType[] cellMap = [NamedCellType.Blocking].cycle.take(mapSize * mapSize).array;

        alias RuleList = RuleSetting!(MapType, MapType)[];
        alias MatchSettingT = MatchSettings!(RuleList, ReturnType!(MapType.IndicesOf));
    }

    public alias MapType = TwoDimensionalIndexer!(0u, CellType[]);

    private
    {
        import std.traits : ReturnType;

        MapType map2D;
        RuleList ruleList;
        MatchSettingT matchSettings = new MatchSettingT();
        string projectName = "Unnamed Project";
    }

    public @property ruleIterationStrategy(MatchSettingT.RuleIterationStrategy s)
    {
        matchSettings.ruleIterationStrategy = s;
    }

    public @property mapIterationStrategy(MatchSettingT.MapIterationStrategy s)
    {
        matchSettings.mapIterationStrategy = s;
    }

    public inout @property iterationCount()
    {
        return matchSettings.iterationCount;
    }

    public auto ref @property iterationCount(size_t newIterationCount)
    {
        matchSettings.iterationCount = newIterationCount;
        return this;
    }

    public this()
    {
        initializeMap;
        initializeRuleList;
    }

    public ~this()
    {
    }

    public ref @property map() inout
    {
        return map2D;
    }

    public auto @property map(MapType newMap)
    {
        map2D = newMap;
        return this;
    }

    public ref @property rules() inout
    {
        return ruleList;
    }

    public auto @property name() const
    {
        return projectName;
    }

    public auto @property rules(typeof(ruleList) newRuleList)
    {
        ruleList = newRuleList;
        return this;
    }

    private auto generateRandomRuleList(const size_t itemCount)
    {
        import std.typecons : tuple;
        import std.random : randomCover;
        import capsicum.patternMatching.RuleSetting : createRuleSetting;

        auto list = [
            createRuleSetting("Sample rule", twoDimensionalIndexer(ruleLeftSide,
                ruleMaxWidth), twoDimensionalIndexer(ruleRightSide, ruleMaxWidth))
        ];
        foreach (const count; iota(1, itemCount + 1))
        {
            list ~= [generateRandomRule()];
        }
        list.randomCover.front.makeUnconditional;
        return list;
    }

    public auto generateRandomRule()
    {
        import std.typecons : tuple;
        import std.random : choice;
        import capsicum.patternMatching.RuleSetting : createRuleSetting;

        auto randomCellPool = generate({ return randomRuleElementPool.choice; });
        auto randomRuleWidthPool = generate({
            return iota(2, randomRuleMaxWidth + 1).choice;
        });
        auto randomRuleHeightPool = generate({
            return iota(2, randomRuleMaxHeight + 1).choice;
        });
        const auto randomRuleWidth = randomRuleWidthPool.front;
        randomRuleWidthPool.popFront;
        const auto randomRuleHeight = randomRuleHeightPool.front;
        randomRuleHeightPool.popFront;
        auto randomLeftSide = randomCellPool.take(ruleMaxWidth * ruleMaxHeight).array;
        auto randomRightSide = randomCellPool.take(ruleMaxWidth * ruleMaxHeight).array;
        return createRuleSetting("Random rule", twoDimensionalIndexer(randomLeftSide,
                ruleMaxWidth)[0 .. randomRuleHeight,
                0 .. randomRuleWidth], twoDimensionalIndexer(randomRightSide,
                ruleMaxWidth)[0 .. randomRuleHeight, 0 .. randomRuleWidth]);
    }

    public void loadFromDirectory(string path)
    {
        import std.path : dirSeparator;

        map2D = deserializeMapFromJSONFile(path ~ dirSeparator ~ mapJsonFileName, map2D);
        ruleList = deserializeRuleListFromJSONFile(path ~ dirSeparator ~ ruleListJsonFileName,
                ruleList);
    }

    private void initializeMap()
    {
        map2D = deserializeMapFromJSONFile(mapJsonFileName,
                twoDimensionalIndexer(cellMap.dup, mapSize));
    }

    private auto deserializeMapFromJSONFile(string path, lazy typeof(map2D) alternative)
    {
        import std.utf : UTFException;
        import std.file : readText, FileException;

        try
        {
            debug write("Loading object from file...");
            debug scope (success)
                writeln("done!");
            debug scope (failure)
                writeln("failed: ");
            immutable string jsonStr = readText(path);
            auto mapJsonObject = jsonStr.parseJSON;
            return twoDimensionalArrayFromJson!CellType(mapJsonObject);
        }
        catch (FileException e)
        {
            debug write("Map JSON file cannot be read");
        }
        catch (std.utf.UTFException e)
        {
            debug write("Error during UTF-decoding of JSON file");
        }
        catch (JSONException e)
        {
            debug write("Error parsing JSON");
        }
        debug writeln(", generating new map...");
        return alternative;
    }

    private void initializeRuleList()
    {
        ruleList = deserializeRuleListFromJSONFile(ruleListJsonFileName,
                generateRandomRuleList(randomRuleCount));
    }

    private auto deserializeRuleListFromJSONFile(string path, lazy typeof(ruleList) alternative)
    {
        import std.algorithm.iteration : map;
        import std.utf : UTFException;
        import std.file : readText, FileException;

        try
        {
            debug write("Loading rule list from file...");
            debug scope (success)
                writeln("done!");
            debug scope (failure)
                writeln("failed: ");
            immutable string jsonStr = readText(path);
            auto ruleListJsonObject = jsonStr.parseJSON;
            return ruleListJsonObject.array.map!(a => createRuleSettingFromJson!CellType(a,
                    ruleMaxWidth, ruleMaxHeight)).array;
        }
        catch (FileException e)
        {
            debug write("Rule list JSON file cannot be read");
        }
        catch (std.utf.UTFException e)
        {
            debug write("Error during UTF-decoding of JSON file");
        }
        catch (JSONException e)
        {
            debug write("Error parsing JSON");
        }
        debug writeln(", generating new rule list...");
        return alternative;
    }

    public void doMatchingStrategy(void delegate(string) statusCallback)
    {
        return doMatchingStrategy(statusCallback, matchSettings);
    }

    public void doMatchingStrategy(void delegate(string) statusCallback,
            MatchSettingT matchingStrategy)
    {
        import std.random : randomCover;
        import std.conv : to;

        bool matched = false;
        statusCallback("Begin matching...");
        foreach (ref iterCount; iota(0u, matchSettings.iterationCount))
        {
            statusCallback("Begin iteration #" ~ iterCount.to!string ~ "/" ~ matchSettings.iterationCount.to!string);
            matched = false;
            foreach (const ruleSetting; matchingStrategy.ruleIterationStrategy(ruleList))
            {
                statusCallback("Matching rule " ~ ruleSetting.name);
                immutable auto rule = ruleSetting.compileRulePattern!LiteralRules;
                foreach (const index; matchingStrategy.mapIterationStrategy(map2D.IndicesOf))
                {
                    auto sliceToBeMatched = map2D[index[0] .. $, index[1] .. $];
                    if (matchRule(rule, sliceToBeMatched))
                    {
                        matched = true;
                        statusCallback("Rule matched!");
                        break;
                    }
                }
                if (matched)
                {
                    break;
                }
            }
            if (!matched)
            {
                statusCallback("Rule set exhausted!");
                break;
            }
        }
    }

    public void doMatchingStrategyAsync(void delegate(string) statusCallback)
    {
        import core.thread;

        new Thread({ doMatchingStrategy(statusCallback); }).start();
        return;
    }

    public bool matchRule(RulePatternT, OutRange)(immutable ref RulePatternT rulePattern,
            OutRange outRange)
    {
        import std.typecons : Yes;

        immutable matchResult = rulePattern.match!(Yes.initValueIsWildcard)(outRange);
        if (matchResult.isSuccessful)
        {
            matchResult.substituteIntoRange!(Yes.initValueIsWildcard)(outRange);
        }
        return matchResult.isSuccessful;
    }

    public void saveToDirectory(string path)
    {
        import std.path : dirSeparator;
        import std.file : mkdirRecurse;
        import std.stdio : toFile;
        import std.algorithm.iteration : map;
        import std.conv : to;

        mkdirRecurse(path);

        debug write("Saving rule list to \"", path ~ dirSeparator ~ ruleListJsonFileName, "\"...");
        auto jsonRuleList = ruleList.map!(to!JSONValue).array.to!JSONValue;
        jsonRuleList.toJSON(false).toFile(path ~ dirSeparator ~ ruleListJsonFileName);
        debug writeln("done!");

        debug write("Saving map to \"", path ~ dirSeparator ~ mapJsonFileName, "\"...");
        JSONValue json2DMap = cast(JSONValue) map2D;
        json2DMap.toJSON(false).toFile(path ~ dirSeparator ~ mapJsonFileName);
        debug writeln("done!");
    }
}
