module capsicum.patternMatching.ruleTypes.Literal;

/** Literal rules match values that are equal to a specific cell value.
  Substitution substitutes the rule value into the matching cell. */
template LiteralRules(CellType, IdType = CellType) if (is(IdType : CellType))
{
    alias DataType = bool;
    alias RuleType = CellType;

    pure bool Match(DataType data, CellType cell, CellType rule) @safe
    {
        return (cell == rule);
    }

    pure CellType Substitute(DataType data, CellType rule)
    {
        return rule;
    }
}
