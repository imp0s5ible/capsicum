module capsicum.patternMatching.ruleTypes.Experimental;

/** [EXPERIMENTAL] Works similar to unbound rules, except the image of the value
  according to a function is matched, instead of the value itself. */
template FunctionalUnboundRules(CellType, IdType = int, ParamType, CellType function(
        CellType, ParamType) fun, CellType function(CellType, ParamType) inverseFun)
{

    alias RuleType = Tuple!(IdType, "id", ParamType, "offset");

    bool Match(ref DataType dictionary, CellType cell, RuleType rule)
    {
        auto actualValue = inverseFun(cell, offset);
        auto cellLocation = rule.id in dictionary;
        if (!cellLocation.isNull)
        {
            return actualValue == *cellLocation;
        }
        else
        {
            dictionary[cell] = actualValue;
        }
    }

    CellType Substitute(const ref DataType dictionary, RuleType rule)
    {
        return fun(dictionary[rule.id], rule.offset);
    }
}

/** [EXPERIMENTAL] Creates an "optional" version of a given rule. Matching the rule always succeeds,
  * but substitution only happens on the cells that matched successfully. */
template OptionalRule(Rules)
{

    alias RuleType = Rules.RuleType;
    alias DataType = Rules.DataType;
    alias CellType = Rules.CellType;

    static if (is(DataType == void))
    {
        bool Match(CellType cell, RuleType rule)
        {
            return true;
        }
    }
    else
    {
        bool Match(CellType cell, RuleType rule, ref DataType data)
        {
            Rules.Match(cell, rule, data);
            return true;
        }
    }

    CellType Substitute(const ref DataType data, RuleType rule)
    {
        return Rules.Substitute(rule);
    }
}
