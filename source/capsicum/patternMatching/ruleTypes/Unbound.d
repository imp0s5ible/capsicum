module capsicum.patternMatching.ruleTypes.Unbound;

/** This rule type represents unbound (or "free") variables, where cells marked with the same id
  must have the same value */
template UnboundRules(CellType, IdType = CellType)
{
    alias DataType = CellType[IdType];
    alias RuleType = IdType;

    pure bool Match(ref DataType dictionary, CellType cell, IdType id) @safe
    {
        auto cellLocation = id in dictionary;
        if (cellLocation !is null)
        {
            return cell == *cellLocation;
        }
        else
        {
            dictionary[id] = cell;
            return true;
        }
    }

    pure CellType Substitute(const ref DataType dictionary, IdType id)
    {
        return dictionary[id];
    }

}

version (unittest)
{
    import capsicum.utils.Unittest;

    /// Using an unbound pattern matcher to reorganize a short array of characters
    unittest
    {
        import std.stdio;
        import std.range;
        import std.conv;
        import capsicum.patternMatching.Engine;

        mixin(unittestDataString("UnboundRules - Matching pattern",
                "Using an unbound pattern matcher to reorganize a short array of characters."));

        immutable string map = "aabb";
        debugLog.writefln("Original map: %s", map);

        immutable int[] pattern = [1, 1, 2, 2];
        immutable int[] replacement = [1, 2, 2, 1, 2, 2];

        alias UnboundStrInt = UnboundRules!(dchar, immutable int);
        auto matchingData = Match!UnboundStrInt(map, pattern);
        assert(!matchingData.isNull);

        immutable string matchedMap = SubstituteIntoNewRange!(UnboundStrInt,
                dchar[], immutable int[])(replacement, matchingData.get).to!string;
        debugLog.writefln("Substituted map: %s", matchedMap);
        assert(matchedMap == "abbabb", "Expected: abbabb / Actual: " ~ matchedMap);
    }

    // This pattern will not match
    unittest
    {
        import capsicum.patternMatching.Engine;

        mixin(unittestDataString("UnboundRules - Non-matching pattern",
                "This pattern should not match."));

        immutable string map = "abcd";
        immutable int[] pattern = [1, 2, 3, 2];

        alias UnboundStrInt = UnboundRules!(dchar, immutable int);
        assert(Match!UnboundStrInt(map, pattern).isNull);
    }

    /** Sparsely populated rules: 
	 * Only the letters whose indices we have rules for will be considered in matching */
    unittest
    {
        import capsicum.patternMatching.Engine;
        import std.stdio;
        import std.conv;
        import std.range;

        mixin(unittestDataString("UnboundRules - Sparsely populated rules",
                "Only the letters whose indices have rules for will be considered in matching."));

        immutable string map = "abcaxc";
        immutable int[int] pattern = [0 : 1, 2 : 2, 3 : 1, 5 : 2,];

        immutable int[] replacement = [1, 2, 1, 2];

        debugLog.writefln("Original map: %s", map);

        alias UnboundStrInt = UnboundRules!(dchar, int);
        const auto matchingData = Match!UnboundStrInt(map, pattern);

        assert(!matchingData.isNull);

        immutable string matchedMap = SubstituteIntoNewRange!(UnboundStrInt,
                dchar[], immutable int[])(replacement, matchingData.get).to!string;
        debugLog.writefln("Substituted map: %s", matchedMap);

        assert(matchedMap == "acac");
    }

    /* Sparsely populated cell grids:
	 * Matching will fail if there is no cell for a given rule.
	 * Even though the pattern here would otherwise match, it won't
	 * because there is a rule for the second letter, but there is no second
	 * letter in the string being matched. */
    unittest
    {
        import capsicum.patternMatching.Engine;

        mixin(unittestDataString("UnboundRules - Sparsely populated cell grids",
                "Matching should fail if there is no cell for a given rule."));

        immutable dchar[ulong] map = [0 : 'H', 2 : 'l', 3 : 'l', 4 : 'o'];

        immutable int[] pattern = [1, 2, 3, 4, 5];

        alias UnboundStrInt = UnboundRules!(dchar, int);

        assert(Match!UnboundStrInt(map, pattern).isNull);
    }
}
