module capsicum.patternMatching.RuleSetting;

import std.json;

debug import std.stdio;

pure class RuleSetting(RuleLeftSideRange, RuleRightSideRange)
{
    private
    {
        RuleLeftSideRange ruleLeftSide;
        RuleRightSideRange ruleRightSide;
        string _name;
    }

    public
    {
        this(string newName, RuleLeftSideRange newLeftSide, RuleRightSideRange newRightSide) pure
        {
            ruleLeftSide = newLeftSide;
            ruleRightSide = newRightSide;
            _name = newName;
        }

        @property inout(string) name() pure inout
        {
            return _name;
        }

        @property inout(RuleLeftSideRange) leftHandSide() pure inout
        {
            return ruleLeftSide;
        }

        @property inout(RuleRightSideRange) rightHandSide() pure inout
        {
            return ruleRightSide;
        }

        @property size_t width() pure const
        {
            return ruleLeftSide.width;
        }

        @property size_t height() pure const
        {
            return ruleLeftSide.height;
        }

        @property ref auto width(size_t newWidth) pure
        {
            ruleLeftSide.width = newWidth;
            ruleRightSide.width = newWidth;
            return this;
        }

        @property ref auto height(size_t newHeight) pure
        {
            ruleLeftSide.height = newHeight;
            ruleRightSide.height = newHeight;
            return this;
        }

        public void makeUnconditional() {
            import std.traits : ForeachType;
            foreach(ref index ; ruleLeftSide.IndicesOf) {
                ruleLeftSide[index] = ForeachType!(RuleLeftSideRange).init;
            }
        }

        template compileRulePattern(alias RuleType)
        {
            import capsicum.patternMatching.RuleSet : CreateRulePattern, CreateRuleVars;

            const auto compileRulePattern() pure inout
            {
                return CreateRulePattern(CreateRuleVars!RuleType(leftHandSide.idup,
                        rightHandSide.idup));
            }
        }

        JSONValue opCast(T : JSONValue)() pure const
        {
            import std.conv : to;

            JSONValue result = ["name" : name];
            result.object["ruleLeftSide"] = ruleLeftSide.to!JSONValue;
            result.object["ruleRightSide"] = ruleRightSide.to!JSONValue;
            return result;
        }
    }
}

public RuleSetting!(RuleLeftSideRange, RuleRightSideRange) createRuleSetting(
        RuleLeftSideRange, RuleRightSideRange)(string name,
        RuleLeftSideRange leftSide, RuleRightSideRange rightSide) pure
{
    return new RuleSetting!(RuleLeftSideRange, RuleRightSideRange)(name, leftSide, rightSide);
}

import capsicum.utils.Indexer;

public RuleSetting!(TwoDimensionalIndexer!(0LU, Elem[]), TwoDimensionalIndexer!(0LU, Elem[])) createRuleSettingFromJson(
        Elem)(JSONValue val, size_t allocateWidth = 3u, size_t allocateHeight = 3u)
{
    import std.conv : to;
    import std.algorithm.iteration : map;
    import std.array;

    return new RuleSetting!(TwoDimensionalIndexer!(0LU, Elem[]),
            TwoDimensionalIndexer!(0LU, Elem[]))(val["name"].str,
            twoDimensionalArrayFromJson!Elem(val["ruleLeftSide"], allocateWidth,
                allocateHeight), twoDimensionalArrayFromJson!Elem(val["ruleRightSide"],
                allocateWidth, allocateHeight));
}
