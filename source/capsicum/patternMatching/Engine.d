module capsicum.patternMatching.Engine;

import std.traits;
import std.functional;
import std.typecons;
import std.range;
import std.algorithm.iteration;
import std.algorithm.searching;

import capsicum.utils.Range;

mixin template PatternMatchingAliases()
{
    alias MatchData = MatchingPolicy.DataType;
    alias MatchRule = MatchingPolicy.Match;

}

mixin template CellTypeAliases()
{
    alias CellType = ForeachType!MapRange;
    static assert(!is(CellType == void), "Cell type of range " ~ MapRange.stringof ~ " is void!");
}

mixin template RuleCellTypeAliases()
{
    alias RuleCellType = ForeachType!RuleRange;

    static assert(!is(RuleCellType == void),
            "Rule cell type of range " ~ RuleRange.stringof ~ " is void!");
}

template Match(alias MatchingPolicy,
        Flag!"initValueIsWildcard" InitValueIsWildcard = No.initValueIsWildcard,
        MapRange, RuleRange)
{
    debug import std.stdio;

    mixin PatternMatchingAliases;
    mixin CellTypeAliases;
    mixin RuleCellTypeAliases;

    enum DontCareValue = CellType.init;

    pure immutable(Nullable!MatchData) Match(const MapRange cellRange, const RuleRange ruleRange) @trusted
    {
        MatchData matchData;
        foreach (const ref index; ruleRange.IndicesOf)
        {
            if (!cellRange.IsValidIndex(index))
            {
                return Nullable!MatchData.init;
            }

            const auto cell = cellRange[index];
            const auto rule = ruleRange[index];
            static if (InitValueIsWildcard)
            {
                if (rule == DontCareValue)
                {
                    continue;
                }
            }

            if (!MatchRule(matchData, cell, rule))
            {
                return Nullable!MatchData.init;
            }
        }
        return Nullable!MatchData(matchData);
    }
}

template Substitute(alias MatchingPolicy, RuleRange)
{
    import capsicum.utils.MapByIndex;

    alias MapRange = ReturnType!(MatchingPolicy.Substitute);
    mixin PatternMatchingAliases;
    mixin RuleCellTypeAliases;

    pure auto Substitute(const RuleRange rule, const ref MatchingPolicy.DataType matchData)
    {
        auto substFun = &partial!(MatchingPolicy.Substitute, matchData);
        return rule.MapByIndex!(substFun, typeof(rule));
    }
}

template SubstituteIntoNewRange(alias MatchingPolicy, OutRange, RuleRange)
{
    import capsicum.utils.MapByIndex;

    alias MapRange = ReturnType!(MatchingPolicy.Substitute);
    mixin PatternMatchingAliases;
    mixin RuleCellTypeAliases;

    /** Returns a new range of the specified type that holds the substituted elements.
		  The range must support the CreateRangeSupportingTheIndicesOf function.
		  The value of elements relevant to the rule range (right hand side of the rule) will be the substituted value.
		  The value and accessibility of elements relevant to the rule range are implementation defined.
		 */
    OutRange SubstituteIntoNewRange(const RuleRange rule, const ref MatchingPolicy.DataType matchData)
    {
        auto substFun = &partial!(MatchingPolicy.Substitute, matchData);
        OutRange result = CreateRangeSupportingTheIndicesOf!OutRange(rule);
        result.CopyByIndexFrom(rule.MapByIndex!(substFun, typeof(rule)));
        return result;
    }
}
