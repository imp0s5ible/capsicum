module capsicum.patternMatching.MatchSettings;

debug import std.stdio : write, writeln;

debug import std.format : format;

import capsicum.Project;

public template IterationStrategy(R)
{
    import std.traits : ForeachType;
    import std.range.interfaces : InputRange, inputRangeObject;
    import std.range : retro, radial;
    import std.functional : pipe, compose;
    import std.random : randomCover;
    import std.array : array;

    alias E = ForeachType!R;

    enum IterationStrategy : InputRange!E function(R)
    {
        Random = function(R r) { return inputRangeObject(r.array.randomCover); },
        Linear = function(R r) { return inputRangeObject(r); },
        Retro = function(R r) { return inputRangeObject(r.array.retro); },
        Radial = function(R r) {
            auto arrayRange = r.array;
            return inputRangeObject(arrayRange.radial(arrayRange.length / 2u));
        },
    }
}

class MatchSettings(RuleRange, MapIndexRange)
{
    import std.functional : toDelegate;

    private
    {
        import std.traits : ForeachType;
        import std.range.interfaces : InputRange;

    }

    public
    {
        alias RuleIterationStrategy = InputRange!(ForeachType!RuleRange) function(RuleRange);
        alias MapIterationStrategy = InputRange!(ForeachType!MapIndexRange) function(MapIndexRange);
        RuleIterationStrategy ruleIterationStrategy = IterationStrategy!RuleRange.Linear;
        MapIterationStrategy mapIterationStrategy = IterationStrategy!MapIndexRange.Linear;

        size_t iterationCount = 1u;
    }

    this()
    {
    }

    this(RuleIterationStrategy r, MapIterationStrategy m)
    {
        ruleIterationStrategy = r;
        mapIterationStrategy = m;
    }

    ~this()
    {
    }
}
