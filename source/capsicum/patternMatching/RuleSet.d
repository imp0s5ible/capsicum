module capsicum.patternMatching.RuleSet;

import std.typecons : Flag, No;

class RuleVars(alias RuleType, RuleRange, SubstitutionRange)
        if (__traits(isTemplate, RuleType))
{

    import std.range.primitives;
    import std.traits;

    import capsicum.patternMatching.Engine;

    private
    {
        import capsicum.utils.Range;

        alias RuleElement = ForeachType!RuleRange;
        alias SubstitutionElement = ForeachType!RuleRange;

        static assert(is(RuleElement : SubstitutionElement),
                "Rule element type " ~ fullyQualifiedName!RuleElement
                ~ " is not implicitly convertible to substitution element type "
                ~ fullyQualifiedName!SubstitutionElement);
        immutable RuleRange ruleRange;
        immutable SubstitutionRange substitutionRange;

        class MatchResult(Element)
        {
            static assert(!is(Element == void), "Element type of MatchResult cannot be void!");
            import std.traits;
            import std.typecons;

            private
            {
                alias MatchingPolicy = RuleType!(Element, RuleElement);
                alias MatchData = Nullable!(MatchingPolicy.DataType);
                immutable MatchData matchData;
            }

            public
            {
                pure immutable bool isSuccessful()
                {
                    return !matchData.isNull;
                }

                private pure immutable this(Range)(Range range,
                        Flag!"initValueIsWildcard" initValueIsWildcard = No.initValueIsWildcard)
                {
                    if (initValueIsWildcard)
                    {
                        matchData = Match!(MatchingPolicy, Yes.initValueIsWildcard)(range,
                                ruleRange);
                    }
                    else
                    {
                        matchData = Match!(MatchingPolicy, No.initValueIsWildcard)(range, ruleRange);
                    }
                }

                pure immutable ref Range substituteIntoRange(
                        Flag!"initValueIsWildcard" InitValueIsWildcard = No.initValueIsWildcard,
                        Range)(ref Range range)
                in
                {
                    import capsicum.utils.Range;
                    import std.conv;

                    assert(isSuccessful,
                            "[RuleSet] Precondition violation: substitution command called on unsuccessful match!");
                    assert(range.SupportsAllIndicesOf(substitutionRange), "[RuleSet] Precondition violation: Output range does not support all indices of substitution range!\nOutput range indices:\n" ~ range
                            .IndicesOf.to!string ~ "\nSubstitution range indices:\n"
                            ~ substitutionRange.IndicesOf.to!string);
                }
                body
                {
                    import capsicum.utils.Range;

                    alias Elem = ElementType!(ReturnType!(ValuesOf!Range));

                    static if (InitValueIsWildcard)
                    {
                        range.CopyByIndexFrom(Substitute!MatchingPolicy(substitutionRange,
                                matchData.get).FilterByIndex!(a => a != Elem.init));
                    }
                    else
                    {
                        range.CopyByIndexFrom(Substitute!MatchingPolicy(substitutionRange,
                                matchData.get));
                    }
                    return range;
                }
            }
        }
    }

    public
    {
        pure immutable this(immutable RuleRange leftHandSide,
                immutable SubstitutionRange rightHandSide)
        {
            ruleRange = leftHandSide;
            substitutionRange = rightHandSide;
        }

        pure immutable auto match(
                Flag!"initValueIsWildcard" InitValueIsWildcard = No.initValueIsWildcard, Range)(
                Range range)
        {
            alias Element = Unqual!(ElementType!(typeof(range.ValuesOf())));
            return this.new immutable(MatchResult!Element)(range, InitValueIsWildcard);
        }
    }
}

pure immutable(RuleVars!(RuleType, RuleRange, SubstitutionRange)) CreateRuleVars(
        alias RuleType, RuleRange, SubstitutionRange)(immutable RuleRange leftHandSide,
        immutable SubstitutionRange rightHandSide)
{
    return new immutable(RuleVars!(RuleType, RuleRange, SubstitutionRange))(
            leftHandSide, rightHandSide);
}

class RulePattern(RuleVars...) if (RuleVars.length <= 2)
{
    import std.range.primitives;
    import std.typecons;

    private
    {
        immutable RuleVars ruleVars;

        class MatchResult(Element)
        {
            alias MatchResultA = RuleVars[0].MatchResult!Element;
            static if (RuleVars.length == 2)
                alias MatchResultB = RuleVars[1].MatchResult!Element;

            immutable MatchResultA matchResultA;
            static if (RuleVars.length == 2)
                immutable MatchResultB matchResultB;

            private pure immutable this(Range)(Range range,
                    Flag!"initValueIsWildcard" initValueIsWildcard = No.initValueIsWildcard)
            {
                if (initValueIsWildcard)
                {
                    matchResultA = ruleVars[0].match!(Yes.initValueIsWildcard, Range)(range);
                }
                else
                {
                    matchResultA = ruleVars[0].match!(No.initValueIsWildcard, Range)(range);
                }
                static if (RuleVars.length == 2)
                {
                    if (initValueIsWildcard)
                    {
                        matchResultB = ruleVars[1].match!(Yes.initValueIsWildcard, Range)(range);
                    }
                    else
                    {
                        matchResultB = ruleVars[1].match!(No.initValueIsWildcard, Range)(range);
                    }
                }
            }

            pure immutable bool isSuccessful()
            {
                static if (RuleVars.length == 2)
                {
                    return matchResultA.isSuccessful && matchResultB.isSuccessful;
                }
                else
                {
                    return matchResultA.isSuccessful;
                }
            }

            pure immutable OutRange generateSubstitutedRange(OutRange,
                    Flag!"initValueIsWildcard" InitValueIsWildcard = No.initValueIsWildcard)()
            {
                import std.traits;
                import capsicum.utils.Range;

                static if (RuleVars.length == 2)
                {
                    Unqual!OutRange result = CreateRangeSupportingTheIndicesOf!(Unqual!OutRange)(
                            ruleVars[0].substitutionRange, ruleVars[1].substitutionRange);
                }
                else
                {
                    Unqual!OutRange result = CreateRangeSupportingTheIndicesOf!(Unqual!OutRange)(
                            ruleVars[0].substitutionRange);
                }
                substituteIntoRange!InitValueIsWildcard(result);
                return result;
            }

            pure immutable ref Range substituteIntoRange(
                    Flag!"initValueIsWildcard" InitValueIsWildcard = No.initValueIsWildcard, Range)(
                    return ref Range range)
            {
                matchResultA.substituteIntoRange!InitValueIsWildcard(range);
                static if (RuleVars.length == 2)
                {
                    matchResultB.substituteIntoRange!InitValueIsWildcard(range);
                }
                return range;
            }
        }
    }

    public
    {
        pure immutable this(immutable RuleVars newRuleVars)
        {
            ruleVars = newRuleVars;
        }

        import std.typecons : Flag, Yes, No;

        pure immutable auto match(
                Flag!"initValueIsWildcard" InitValueIsWildcard = No.initValueIsWildcard, Range)(
                Range range)
        {
            import capsicum.utils.Range;
            import std.traits;

            alias Element = Unqual!(ElementType!(typeof(range.ValuesOf())));

            return this.new immutable(MatchResult!Element)(range, InitValueIsWildcard);
        }
    }
}

pure immutable(RulePattern!RuleVars) CreateRulePattern(RuleVars...)(immutable RuleVars ruleVars)
{
    return new immutable(RulePattern!RuleVars)(ruleVars);
}

version (unittest)
{

    unittest
    {
        import std.stdio;
        import std.range;
        import std.conv;

        import capsicum.patternMatching.ruleTypes.Literal;
        import capsicum.patternMatching.ruleTypes.Unbound;

        import capsicum.utils.Unittest;

        mixin(unittestDataString("RuleSet", "Normal rule pattern"));

        immutable unboundLeftSide = [0 : 'a', 2 : 'b', 4 : 'a', 6 : 'b'];
        immutable unboundRightSide = [1 : 'a', 3 : 'a', 5 : 'b', 7 : 'b'];

        immutable literalLeftSide = [1 : 'f', 3 : 'g', 5 : 'h', 7 : 'i'];
        immutable literalRightSide = [0 : 'u', 2 : 'v', 4 : 'w', 6 : 'x'];

        import std.traits;
        import capsicum.utils.Range;

        immutable myRule = CreateRulePattern(CreateRuleVars!UnboundRules(unboundLeftSide,
                unboundRightSide), CreateRuleVars!LiteralRules(literalLeftSide,
                literalRightSide));

        immutable pattern = "xfygxhyi";
        debugLog.writefln("Original pattern: %s", pattern);

        immutable matchResult = myRule.match(pattern);

        debugLog.writefln("%s", matchResult.isSuccessful ? "Match successful!" : "Match failed!");
        assert(matchResult.isSuccessful);

        immutable substitutedPattern = matchResult.generateSubstitutedRange!(dchar[]).to!string;
        debugLog.writefln("Substituted pattern: %s (%s characters)",
                substitutedPattern, substitutedPattern.length);
        assert(substitutedPattern == "uxvxwyxy");
    }
}
