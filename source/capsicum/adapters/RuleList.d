module capsicum.adapters.RuleList;

import capsicum.adapters.CellAdapter;
import capsicum.adapters.RuleEditor;
import capsicum.patternMatching.RuleSetting;
import capsicum.utils.Range;

import std.traits;
import std.range.primitives;

debug import std.stdio : writeln;

version (gtkd)
{
    import gtk.Widget;
    import gtk.Grid;
    import gtk.Viewport;
    import gtk.ListBox;
    import gtk.ListBoxRow;
    import gtk.ScrolledWindow;
    import gtk.Adjustment;
    import gtk.Button;

    import capsicum.adapters.MapEditor;

    import std.typecons : Tuple;

    class RuleList(RangeType, RuleCollection = RuleSetting!(RangeType, RangeType)[]) : Grid
    {
        private
        {
            import std.range.primitives : ElementType;
            import capsicum.patternMatching.RuleSetting : RuleSetting;
            import capsicum.utils.Range;

            alias Rule = ForeachType!RuleCollection;
            alias Elem = ForeachType!RangeType;
            static assert(!is(Elem == void));

            ScrolledWindow listBoxViewport;
            ListBox ruleListBox;
            Button addButton;
            Button removeButton;
            RuleCollection* _ruleCollection;
            MapEditor!RangeType _connectedMapEditor = null;
            Unqual!Elem[string] valueSetterMenuElems;

            Rule delegate() _ruleGenerator;
        }

        public @property ruleGenerator(Rule delegate() newRuleGenerator)
        {
            _ruleGenerator = newRuleGenerator;
        }

        public @property size_t ruleCount() const
        {
            return ruleCollection.length;
        }

        public void selectRuleByIndex(size_t ind)
        {
            if (auto newSelectedRow = ruleListBox.getRowAtIndex(cast(int) ind))
            {
                ruleListBox.selectRow(newSelectedRow);
            }
        }

        private void rowSelectionHandler(ListBoxRow lbr, ListBox lb)
        {
            cast(void) lb;
            if (lbr is null)
            {
                _connectedMapEditor.selectedRuleSetting = null;
                return;
            }
            _connectedMapEditor.selectedRuleSetting = *(((cast(RuleEditorWidget!(RangeType,
                    RangeType)) lbr.getChild).ruleSetting));
            assert(_connectedMapEditor.selectedRuleSetting !is null);
        };

        this(ref MapEditor!RangeType mapEditor, ref RuleCollection newRuleCollection)
        {
            _ruleCollection = &newRuleCollection;
            _connectedMapEditor = mapEditor;

            setupListBoxViewport;
            setupControlButtons;
            setupGeneralEventHandlers;

            setupSpacing;
            attachWidgets;
        }

        private
        {
            void setupListBoxViewport()
            {
                auto listBoxViewportAdjustment = new Adjustment(0.0, 0.0, 300.0, 10.0, 10.0, 10.0);
                listBoxViewport = new ScrolledWindow(null, null);
                listBoxViewport.setPolicy(GtkPolicyType.NEVER, GtkPolicyType.AUTOMATIC);
                listBoxViewport.setVexpand(true);
                regenerateEditorList;
                ruleListBox.addOnRowSelected(&rowSelectionHandler);
            }

            void setupControlButtons()
            {
                setupAddButton;
                setupRemoveButton;
            }

            void setupAddButton()
            {
                addButton = new Button("New");
                addButton.addOnClicked((Button bn) { cast(void) bn; insertRule; });
            }

            void setupRemoveButton()
            {
                removeButton = new Button("Delete");
                removeButton.addOnClicked((Button bn) {
                    cast(void) bn;
                    auto selectedRow = ruleListBox.getSelectedRow;
                    if (selectedRow !is null)
                    {
                        removeRuleAtIndex(selectedRow.getIndex);
                    }
                });
            }

            void setupGeneralEventHandlers()
            {
                addEvents(EventMask.FOCUS_CHANGE_MASK | EventMask.LEAVE_NOTIFY_MASK);
                addOnFocusOut((GdkEventFocus* e, Widget w) {
                    cast(void) w;
                    cast(void) e;
                    debug writeln("Rule list focus out!");
                    ruleListBox.selectRow(null);
                    return false;
                });
            }

            void setupSpacing()
            {
                setMarginTop(10u);
                setMarginBottom(10u);
                setMarginLeft(10u);
                setMarginRight(10u);
                setColumnSpacing(10u);
                setRowSpacing(10u);
            }

            void attachWidgets()
            in
            {
                assert(listBoxViewport !is null,
                        "[RULE LIST]: List box viewport has not been set up!");
                assert(addButton !is null, "[RULE LIST]: Add button has not been set up!");
                assert(removeButton !is null, "[RULE LIST]: Remove has not been set up!");
            }
            body
            {
                removeAll;
                attach(listBoxViewport, 0, 0, 2, 1);
                attach(addButton, 0, 1, 1, 1);
                attach(removeButton, 1, 1, 1, 1);
            }
        }

        auto ref removeRuleAtIndex(int index)
        {
            import std.algorithm : remove;

            *_ruleCollection = (*_ruleCollection).remove(index);
            regenerateEditorList;
            if (auto newSelectedRow = ruleListBox.getRowAtIndex(index))
            {
                ruleListBox.selectRow(newSelectedRow);
            }
            else if (auto newSelectedRow = ruleListBox.getRowAtIndex(index - 1))
            {
                ruleListBox.selectRow(newSelectedRow);
            }
            return this;
        }

        auto ref insertRule()
        {
            if (&_ruleGenerator !is null)
            {
                *_ruleCollection ~= [_ruleGenerator()];
            }
            else
            {
                debug writeln(
                        "WARNING: New rule button clicked, but no rule generator was specified!");
            }
            regenerateEditorList;
            return this;
        }

        public void regenerateEditorList()
        {
            import std.conv : to;

            if (ruleListBox !is null)
            {
                ruleListBox.removeAll;
            }
            else
            {
                ruleListBox = new ListBox();
                listBoxViewport.add(ruleListBox);
            }
            foreach (const ref index, ref rule; *_ruleCollection)
            {
                auto ruleEditor = ruleListItem(rule);
                ruleEditor.cellSize = 16;
                ruleEditor.addNamedValuesToValueSetterMenu(valueSetterMenuElems);
                ruleListBox.insert(ruleEditor, cast(int) index);
            }
            ruleListBox.setVexpand(true);
            ruleListBox.showAll;
            if (listBoxViewport !is null)
            {
                listBoxViewport.setVexpand(true);
            }
        }

        public const(RuleCollection) ruleCollection() const
        {
            return *_ruleCollection;
        }

        auto ref addEnumValuesToValueSetterMenu(EnumT)()
        {
            import std.traits : EnumMembers;
            import std.conv : to;

            foreach (val; EnumMembers!EnumT)
            {
                string name = val.to!string;
                valueSetterMenuElems[name] = val;
            }
            regenerateEditorList;
            return this;
        }

        ~this()
        {
        }
    }
}

template createRuleList(RangeType, RuleCollection = RuleSetting!(RangeType, RangeType)[])
{
    auto createRuleList(MapEditor!RangeType mapEditor, ref RuleCollection ruleCollection)
    {
        return new RuleList!RangeType(mapEditor, ruleCollection);
    }
}
