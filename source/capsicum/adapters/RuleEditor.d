module capsicum.adapters.RuleEditor;

import capsicum.patternMatching.RuleSetting;

debug import std.stdio;

version (gtkd)
{
    import gtk.Grid;

    import std.traits;

    template RuleEditorWidget(RuleLeftSideRange, RuleRightSideRange) /*if (is(typeof(RuleLeftSideRange[size_t, size_t])) &&
			is(typeof(RuleRightSideRange[size_t, size_t])) &&
			hasMember!(RuleLeftSideRange,"width") && hasMember!(RuleRightSideRange,"width") &&
			hasMember!(RuleLeftSideRange,"height") && hasMember!(RuleRightSideRange,"height")) */
    {
        import capsicum.adapters.CellAdapter;
        import gtk.Label;

        class RuleEditorWidget : Grid
        {
            import gtk.SpinButton : SpinButton;

            private
            {
                import std.range.primitives;

                alias LeftSideElem = ElementType!(typeof(_ruleSetting.leftHandSide.ValuesOf()));
                alias RightSideElem = ElementType!(typeof(_ruleSetting.rightHandSide.ValuesOf()));

                RuleSetting!(RuleLeftSideRange, RuleRightSideRange)* _ruleSetting;
                int _cellSize = 32;

                CellGrid!RuleLeftSideRange ruleLeftSideGrid;
                CellGrid!RuleRightSideRange ruleRightSideGrid;

                SpinButton widthSpinner;
                SpinButton heightSpinner;
            }

            public
            {
                enum Mode
                {
                    Preview,
                    Edit
                }

                @property auto ref cellSize(const int newCellSize)
                {
                    import std.algorithm.comparison : max;

                    ruleLeftSideGrid.cellSize = newCellSize;
                    ruleRightSideGrid.cellSize = newCellSize;
                    setSizeRequest(-1, cast(int)(max(ruleLeftSideGrid.height,
                            ruleRightSideGrid.height) * newCellSize));
                    return this;
                }

                auto ref addEnumValuesToValueSetterMenu(EnumT)()
                {
                    ruleLeftSideGrid.addEnumValuesToValueSetterMenu!EnumT;
                    ruleRightSideGrid.addEnumValuesToValueSetterMenu!EnumT;
                    return this;
                }

                auto ref addNamedValuesToValueSetterMenu(LeftSideElem[string] pairs)
                {
                    ruleLeftSideGrid.addNamedValuesToValueSetterMenu(pairs);
                    ruleRightSideGrid.addNamedValuesToValueSetterMenu(pairs);
                    return this;
                }

                @property size_t width() const
                {
                    return _ruleSetting.width;
                }

                @property ref auto width(size_t newWidth)
                {
                    ruleLeftSideGrid.width = newWidth;
                    ruleRightSideGrid.width = newWidth;
                    _ruleSetting.width = newWidth;
                    return this;
                }

                @property size_t height() const
                {
                    return _ruleSetting.height;
                }

                @property auto ref height(size_t newHeight)
                {
                    ruleLeftSideGrid.height = newHeight;
                    ruleRightSideGrid.height = newHeight;
                    _ruleSetting.height = newHeight;
                    return this;
                }

                @property auto name() pure const
                {
                    return _ruleSetting.name;
                }

                @property ref ruleSetting()
                {
                    return _ruleSetting;
                }

                this(ref RuleSetting!(RuleLeftSideRange, RuleRightSideRange) newRuleSetting)
                {
                    _ruleSetting = &newRuleSetting;

                    setupRuleGrids;
                    setupSizeSpinners;

                    attachWidgets;
                }
            }

            private
            {
                void setupRuleGrids()
                {
                    setupRuleGrid(ruleLeftSideGrid, _ruleSetting.leftHandSide);
                    setupRuleGrid(ruleRightSideGrid, _ruleSetting.rightHandSide);
                }

                void setupRuleGrid(Range)(ref CellGrid!Range grid, Range rng)
                {
                    grid = new CellGrid!Range(rng, EditingPolicy.Editable);
                    with (grid)
                    {
                        editable = true;
                        cellSize = this._cellSize;
                    }
                }

                void setupSizeSpinners()
                {
                    setupWidthSpinner;
                    setupHeightSpinner;
                }

                void setupWidthSpinner()
                {
                    widthSpinner = new SpinButton(1.0, ruleLeftSideGrid.maxWidth, 1.0);
                    widthSpinner.setValue(cast(double) ruleLeftSideGrid.width);
                    widthSpinner.addOnValueChanged((SpinButton sb) {
                        int value = sb.getValueAsInt;
                        ruleLeftSideGrid.width = value;
                        ruleRightSideGrid.width = value;
                        _ruleSetting.width = value;
                    });
                }

                void setupHeightSpinner()
                {
                    heightSpinner = new SpinButton(1.0, ruleLeftSideGrid.maxHeight, 1.0);
                    heightSpinner.setValue(cast(double) ruleLeftSideGrid.height);
                    heightSpinner.addOnValueChanged((SpinButton sb) {
                        int value = sb.getValueAsInt;
                        ruleLeftSideGrid.height = value;
                        ruleRightSideGrid.height = value;
                        _ruleSetting.height = value;
                    });
                }

                void attachWidgets()
                in
                {
                    assert(ruleLeftSideGrid !is null,
                            "[RULE_EDITOR]: Left hand side grid has not been initialized!");
                    assert(ruleRightSideGrid !is null,
                            "[RULE_EDITOR]: Right hand side grid has not been initialized!");
                    assert(widthSpinner !is null,
                            "[RULE_EDITOR]: Width spinner has not been initialized!");
                    assert(heightSpinner !is null,
                            "[RULE_EDITOR]: Height spinner has not been initialized!");
                }
                body
                {
                    removeAll;
                    attach(new Label(name), 0, 0, 3, 1);
                    attach(ruleLeftSideGrid, 0, 1, 1, 1);
                    attach(new Label(" ---> "), 1, 1, 1, 1);
                    attach(ruleRightSideGrid, 2, 1, 1, 1);

                    attach(new Label("Width:"), 3, 0, 1, 1);
                    attach(widthSpinner, 4, 0, 1, 1);
                    attach(new Label("Height:"), 3, 1, 1, 1);
                    attach(heightSpinner, 4, 1, 1, 1);
                }
            }
        }
    }

    auto ruleListItem(RuleLeftSideRange, RuleRightSideRange)(
            ref RuleSetting!(RuleLeftSideRange, RuleRightSideRange) ruleSetting)
    {
        return new RuleEditorWidget!(RuleLeftSideRange, RuleRightSideRange)(ruleSetting);
    }
}
