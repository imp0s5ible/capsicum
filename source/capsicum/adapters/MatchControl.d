module capsicum.adapters.MatchControl;

debug import std.stdio : write, writeln;

debug import std.format : format;

import capsicum.Project;
import capsicum.adapters.ProjectWindow;
import capsicum.patternMatching.MatchSettings;
import capsicum.patternMatching.RuleSetting;

import capsicum.utils.Range;
import capsicum.utils.Enum;

import std.traits : ReturnType;
import std.conv : to;

version (gtkd)
{
    import gtk.Grid;
    import gtk.Button;
    import gtk.SpinButton;
    import gtk.Label;
    import gtk.ComboBoxText;

    class MatchControl : Grid
    {
        private
        {
            Button matchButton;
            Label matchStatusIndicator;
            SpinButton iterationCountSpinner;
            ProjectWindow linkedWindow;

            ComboBoxText ruleIterationStrategySelector;
            alias RuleIterationStrategy = IterationStrategy!((RuleSetting!(Project.MapType,
                    Project.MapType))[]);
            static RuleIterationStrategy[string] ruleIterationStrategyMap;
            ComboBoxText mapIterationStrategySelector;
            alias MapIterationStrategy = IterationStrategy!(ReturnType!(Project.MapType.IndicesOf));
            static MapIterationStrategy[string] mapIterationStrategyMap;
        }

        static this()
        {
            foreach (name, value; NameValuePairs!RuleIterationStrategy)
            {
                ruleIterationStrategyMap[name] = value;
            }
            foreach (name, value; NameValuePairs!MapIterationStrategy)
            {
                mapIterationStrategyMap[name] = value;
            }
        }

        this(ProjectWindow w)
        {
            linkedWindow = w;
            setupMatchStatusIndicator;
            setupMatchButton;
            setupRuleIterationStrategySelector;
            setupMapIterationStrategySelector;
            setupIterationCountSpinner;
            attachWidgets;
            setupLayout;
        }

        private void setupMatchButton()
        {
            matchButton = new Button("Begin Matching");
            matchButton.addOnClicked((Button bn) {
                cast(void) bn;
                with (linkedWindow)
                {
                    setSensitive(false);
                    project.doMatchingStrategy((string s) {
                        matchStatusIndicator.setText(s);
                    });
                    setSensitive(true);
                    mapEditor.redraw;
                }
            });
        }

        private void setupMatchStatusIndicator()
        {
            matchStatusIndicator = new Label("Matcher ready...");
        }

        private void setupRuleIterationStrategySelector()
        {
            ruleIterationStrategySelector = new ComboBoxText();
            with (ruleIterationStrategySelector)
            {
                addOnChanged((ComboBoxText comboBox) {
                    linkedWindow.project.ruleIterationStrategy = ruleIterationStrategyMap.get(comboBox.getActiveText,
                        RuleIterationStrategy.init);
                });
                foreach (name; __traits(allMembers, RuleIterationStrategy))
                {
                    appendText(name);
                    setActive(0);
                }
            }
        }

        private void setupMapIterationStrategySelector()
        {
            mapIterationStrategySelector = new ComboBoxText();
            with (mapIterationStrategySelector)
            {
                addOnChanged((ComboBoxText comboBox) {
                    linkedWindow.project.mapIterationStrategy = mapIterationStrategyMap.get(comboBox.getActiveText,
                        MapIterationStrategy.init);
                });
                foreach (name; __traits(allMembers, MapIterationStrategy))
                {
                    mapIterationStrategySelector.appendText(name);
                    mapIterationStrategySelector.setActive(0);
                }
            }
        }

        private void setupIterationCountSpinner()
        {
            iterationCountSpinner = new SpinButton(1.0, 1000.0, 1.0);
            iterationCountSpinner.setValue(cast(double) linkedWindow.project.iterationCount);
            iterationCountSpinner.addOnValueChanged((SpinButton sb) {
                size_t value = cast(size_t)sb.getValueAsInt;
                linkedWindow.project.iterationCount = value;
            });
        }

        private void attachWidgets()
        {
            attach(matchButton, 0, 0, 1, 2);
            attach(new Label("Rule iteration strategy:"), 1, 0, 1, 1);
            attach(ruleIterationStrategySelector, 2, 0, 1, 1);
            attach(new Label("Map iteration strategy:"), 1, 1, 1, 1);
            attach(mapIterationStrategySelector, 2, 1, 1, 1);
            attach(new Label("Iteration count:"), 1, 2, 1, 1);
            attach(iterationCountSpinner, 2, 2, 1, 1);
            attach(matchStatusIndicator, 0, 3, 3, 1);
        }

        private void setupLayout()
        {
            setMarginTop(10u);
            setMarginBottom(10u);
            setMarginLeft(10u);
            setMarginRight(10u);
            setColumnSpacing(10u);
            setRowSpacing(10u);
            setHexpand(true);
            setVexpand(true);
        }

        ~this()
        {
        }
    }
}
