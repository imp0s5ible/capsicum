module capsicum.adapters.MapEditor;

debug import std.stdio : writeln;

debug import std.format : format;

version (gtkd)
{
    import gtk.Grid : Grid;

    /**
      * A preset Layout class that contains a CellGrid view of a two dimensional collection,
      * as well as some tools to facilitate editing.
      */
    class MapEditor(ElemRange) : Grid
    {
        import capsicum.utils.Range;
        import capsicum.adapters.CellAdapter;
        import capsicum.patternMatching.RuleSetting;

        import std.range.primitives;
        import std.traits;

        import gtk.Adjustment;
        import gtk.ScrolledWindow : ScrolledWindow;
        import gtk.ComboBoxText;
        import gtk.Button;

        private
        {
            alias Elem = Unqual!(ElementType!(ReturnType!(ElemRange.ValuesOf)));
            Elem[string] valueList;

            ElemRange* _map;
            RuleSetting!(ElemRange, ElemRange) _selectedRuleSetting = null;

            ScrolledWindow mapViewport;
            CellGrid!ElemRange mapGrid;
            ComboBoxText valueSelector;
            Button fillButton;
            Button matchSingleButton;
            Button clearButton;

            enum int DefaultCellSize = 16;
        }

        this(ref ElemRange newMap)
        {
            _map = &newMap;
            setupMapViewport;
            setupToolWidgets;

            setupSpacing;
            attachWidgets;
        }

        private void setupMapViewport()
        {
            mapGrid = new CellGrid!ElemRange(*_map, EditingPolicy.Editable).cellSize(
                    DefaultCellSize);

            auto viewPortAdjustment = new Adjustment(0.0, 0.0,
                    _map.width * cellSize, _map.width, _map.height, _map.height * cellSize);
            mapViewport = new ScrolledWindow(viewPortAdjustment, viewPortAdjustment);
            with (mapViewport)
            {
                setPolicy(GtkPolicyType.AUTOMATIC, GtkPolicyType.AUTOMATIC);
                add(mapGrid);
                setSizeRequest(512, 512);
            }
        }

        private void setupToolWidgets()
        {
            setupValueSelector;
            setupFillButton;
            setupMatchButton;
            setupClearButton;
        }

        private void setupValueSelector()
        {
            valueSelector = new ComboBoxText();
        }

        private void setupFillButton()
        {
            fillButton = new Button("Fill selection");
            fillButton.addOnClicked((Button bn) {
                cast(void) bn;
                Elem fillValue = valueList.get(valueSelector.getActiveText, Elem.init);
                foreach (index; mapGrid.selectionRange.IndicesOf)
                {
                    mapGrid.selectionRange[index] = fillValue;
                }
                mapGrid.redraw;
            });
        }

        private void setupMatchButton()
        {
            matchSingleButton = new Button("Match selected rule");
            matchSingleButton.addOnClicked((Button bn) {
                cast(void) bn;
                matchSelectedRule;
            });
            matchSingleButton.setSensitive(false);
        }

        private void setupClearButton()
        {
            clearButton = new Button("Clear map");
            clearButton.addOnClicked((Button bn) {
                cast(void) bn;
                Elem fillValue = valueList.get(valueSelector.getActiveText, Elem.init);
                foreach (index; mapGrid.collectionBehind.IndicesOf)
                {
                    mapGrid.collectionBehind[index] = fillValue;
                }
                mapGrid.redraw;
            });
        }

        private void setupSpacing()
        {
            setMarginTop(10u);
            setMarginBottom(10u);
            setMarginLeft(10u);
            setMarginRight(10u);
            setColumnSpacing(10u);
            setRowSpacing(10u);
        }

        private void attachWidgets()
        {
            removeAll;
            attach(valueSelector, 0, 0, 1, 1);
            attach(fillButton, 1, 0, 1, 1);
            attach(matchSingleButton, 2, 0, 1, 1);
            attach(clearButton, 3, 0, 1, 1);
            attach(mapViewport, 0, 1, 4, 1);
            setSizeRequest(512, 512);
        }

        /**
          * Convenience function to add the names and values to the MapEditor's value selector list.
          */
        public ref auto addEnumValuesToValueList(EnumT)()
        {
            import capsicum.utils.Enum;

            foreach (name, value; NameValuePairs!EnumT)
            {
                valueList[name] = value;
            }
            regenerateValueSelector;
            return this;
        }

        private void regenerateValueSelector()
        {
            valueSelector.removeAll;
            foreach (name, val; valueList)
            {
                valueSelector.appendText(name);
                valueSelector.setActive(0);
            }
        }

        /**
          * The width and height in pixels of the cell displayed to represent one element.
          */
        public @property auto cellSize() inout
        {
            return mapGrid.cellSize;
        }

        /**
          * Set the width and height in pixels of the cell displayed to represent one element.
          */
        public @property ref auto cellSize(int newCellSize)
        {
            mapGrid.cellSize = newCellSize;
            return this;
        }

        /**
          * Sets the selected rule setting to be applied when pressing the apply selected rule button.
          */
        public ref auto @property selectedRuleSetting(RuleSetting!(ElemRange,
                ElemRange) newRuleSetting)
        {
            _selectedRuleSetting = newRuleSetting;
            if (_selectedRuleSetting !is null)
            {
                mapGrid.fixedSelectionSize(_selectedRuleSetting.width, _selectedRuleSetting.height);
                mapGrid.redraw;
            }
            else
            {
                mapGrid.hasSelection = false;
            }
            matchSingleButton.setSensitive(_selectedRuleSetting !is null);
            return this;
        }

        public @property selectedRuleSetting() const
        {
            return _selectedRuleSetting;
        }

        /**
          * Compiles and matches the selected rule over the map's active selection.
          * Matching fails automatically if there is no selected rule, or if the map has no active selection.
          * Returns: True if the match succeeded, false if it failed.
          */
        public bool matchSelectedRule()
        {
            import capsicum.patternMatching.ruleTypes.Literal;

            if (_selectedRuleSetting is null) {
                return false;
            }

            immutable auto localRule = _selectedRuleSetting.compileRulePattern!LiteralRules;
            return matchRule(localRule);
        }

        /**
          * Matches a given rule against the map's active selection.
          * Matching fails automatically if there map has no active selection.
          * Returns: True if the match succeeded, false if it failed.
          */
        public bool matchRule(RulePatternT)(immutable ref RulePatternT rulePattern)
        {
            import std.typecons : Yes;

            if (!mapGrid.hasSelection)
            {
                return false;
            }

            immutable matchResult = rulePattern.match!(
                    Yes.initValueIsWildcard)(mapGrid.selectionRange);
            if (matchResult.isSuccessful)
            {
                auto result2d = mapGrid.selectionRange;
                matchResult.substituteIntoRange!(Yes.initValueIsWildcard)(result2d);
                mapGrid.redraw;
            }
            return matchResult.isSuccessful;
        }

        public auto opDispatch(string name, Args...)(Args args)
        {
            mixin("return mapGrid." ~ name ~ "(args);");
        }
    }
}
