module capsicum.adapters.CellAdapter;

debug import std.stdio : writeln;

version (gtkd)
{
    import gtk.Widget;
    import gtk.DrawingArea;
    import cairo.Context;

    import std.typecons;
    import gobject.Signals;
    import gobject.ObjectG;
    import gobject.ParamSpec;

    alias CellGridBase = DrawingArea;
}

enum EditingPolicy
{
    Editable,
    Readonly
}

/**
 Graphical widget that allows for the display and interaction with two dimensional collections.

 CollectionOfE - Must provide width, height and getDrawColor primitives.
*/
class CellGrid(CollectionOfE) : CellGridBase
{
    private
    {
        import std.range.primitives : ElementType;
        import std.traits : ReturnType;

        alias Elem = ElementType!(ReturnType!(CollectionOfE.ValuesOf));

        /**
            A simple selection class representing a selection over a CellGrid.
            Its root and end can be controlled manually, and the top/left bottom/right positions are calculated
            automatically, regardless of the relative positions of the root and end.

            Since the Selection class instance is bound to a CellGrid instance, it can also internally verify
            via class invariatns the validity of the selection so that it never points outside the CellGrid.
        */
        class Selection(Index)
        {
            private
            {
                alias PositionType = Tuple!(Index, Index);
                alias SizeType = PositionType;

                PositionType _root = tuple(0u, 0u);
                PositionType _end = tuple(0u, 0u);
                SizeType _fixedSize = tuple(0u, 0u);

                invariant
                {
                    import std.conv : to;

                    assert(_fixedSize[0] <= numberOfRows);
                    assert(_fixedSize[1] <= numberOfColumns);
                    assert(0 <= _root[0] && _root[0] < numberOfRows,
                            "Selection root[0] = " ~ _root[0].to!string
                            ~ " is not between 0 and " ~ numberOfRows.to!string ~ "!");
                    assert(0 <= _root[1] && _root[1] < numberOfColumns,
                            "Selection root[1] = " ~ _root[1].to!string
                            ~ " is not between 0 and " ~ numberOfColumns.to!string ~ "!");
                    assert(0 <= _end[0] && _end[0] < numberOfRows,
                            "Selection end[0] = " ~ _end[0].to!string
                            ~ " is not between 0 and " ~ numberOfRows.to!string ~ "!");
                    assert(0 <= _end[1] && _end[1] < numberOfColumns,
                            "Selection end[1] = " ~ _end[1].to!string
                            ~ " is not between 0 and " ~ numberOfColumns.to!string ~ "!");
                }
            }

            public
            {
                /**
                 * Sets the root of the selection. This is what is set when the selection begins, so this also sets
                 * the end of the selection to the appropriate position, which is the same as the root position
                 * when it is not the fixed size, and offset to the bottom left by the appropriate size when it is.
                 *
                 * When the selection has a fixed size, the root might also be placed to the closest available position,
                 * if placing it at the requested position would result in an invalid (pointing outside the grid)
                 * selection.
                */
                @property Selection!Index root(const PositionType newRoot)
                {
                    _root[0] = (newRoot[0] + _fixedSize[0] <= numberOfRows) ? max(newRoot[0], 0)
                        : (numberOfRows - _fixedSize[0]);
                    _root[1] = (newRoot[1] + _fixedSize[1] <= numberOfColumns) ? max(newRoot[1], 0)
                        : (numberOfColumns - _fixedSize[1]);
                    _end = tuple(_root[0] + getOffsetFromSize(_fixedSize[0]),
                            _root[1] + getOffsetFromSize(_fixedSize[1]));
                    return this;
                }

                private size_t getOffsetFromSize(size_t size)
                {
                    return 0u < size ? (size - 1u) : 0u;
                }

                /**
                 * Sets the end of the selection. This is what is set when the selection is ongoing, e.g. when the
                 * mouse button is pressed and dragged.
                 * When the position is fixed, this is the same as setting the root, otherwise the root is not affected.
                 * The end may be clamped to within the CellGrid limits to uphold the invariant.
                */
                @property ref auto end(const PositionType newEnd)
                {
                    if (isFixedSize)
                    {
                        root = newEnd;
                    }
                    else
                    {
                        import std.algorithm.comparison : clamp;

                        _end[0] = clamp(newEnd[0], 0u, numberOfRows - 1);
                        _end[1] = clamp(newEnd[1], 0u, numberOfColumns - 1);
                    }
                    return this;
                }

                /**
                 * Sets a fixed size for this selection.
                 * Selecting a fixed size that has a width or height of zero makes the selection
                 * unlimited again.
                */
                ref auto setFixedSize(const ref Index width, const ref Index height)
                {
                    _fixedSize = tuple(height, width);
                    if (isFixedSize)
                    {
                        root = _root;
                    }
                    return this;
                }

                /**
                 * The width of the current selection, in the number of elements.
                */
                @property Index width() const
                {
                    return right - left;
                }

                /**
                 * The height of the current selection, in the number of elements.
                 */
                @property Index height() const
                {
                    return bottom - top;
                }

                import std.algorithm : min, max;

                /**
                 * The topmost coordinate of the selection, automatically calculated from the root and the end.
                 */
                @property top() const
                {
                    return min(_root[0], _end[0]);
                }

                /**
                 * The bottom-most coordinate of the selection, automatically calculated from the root and the end.
                 */
                @property bottom() const
                {
                    return max(_root[0], _end[0]);
                }

                /**
                 * The leftmost coordinate of the selection, automatically calculated from the root and the end.
                 */
                @property left() const
                {
                    return min(_root[1], _end[1]);
                }

                /**
                 * The rightmost coordinate of the selection, automatically calculated from the root and the end.
                 */
                @property right() const
                {
                    return max(_root[1], _end[1]);
                }

                /**
                 * Whether the selection is of fixed size or not.
                 * True if the size is fixed, false if the size is unrestricted.
                 */
                @property bool isFixedSize() const
                {
                    return _fixedSize[0] != 0u && _fixedSize[1] != 0u;
                }

                /**
                  * Returns the selection to a default state of the top left cell being selected with
                  * unrestricted selection.
                  */
                ref auto clear()
                {
                    _fixedSize = tuple(cast(Index) 0u, cast(Index) 0u);
                    _root = tuple(cast(Index) 0u, cast(Index) 0u);
                    _end = tuple(cast(Index) 0u, cast(Index) 0u);
                    return this;
                }
            }
        }

        CollectionOfE gridCollection;
        int _cellSize = 16;

        version (gtkd)
        {
            import gtk.Menu : Menu;
            import gtk.MenuItem : MenuItem;

            Menu valueSetterMenu;
        }

        void platformSpecificInit()
        {
            version (dlangui)
            {
                defColumnWidth = 32;
                defRowHeight = 32;
                showColHeaders = false;
                showRowHeaders = false;
                resize(numberOfColumns, numberOfRows);
            }
            else version (gtkd)
            {
                addOnDraw((Scoped!Context cairoContext, Widget wg) {
                    CellGrid cellGrid = cast(CellGrid) wg;
                    assert(cellGrid);
                    cellGrid.draw(cairoContext);
                    return true;
                });
            }
        }

        void addEditingEventHandlers()
        {
            version (dlangui)
            {
            }
            else version (gtkd)
            {

                addOnButtonPress((GdkEventButton* eventButton, Widget wg) {
                    cast(void) wg;
                    if (!editable)
                    {
                        return false;
                    }

                    if (eventButton.button == 1)
                    {
                        ensureSelectionExists;
                        const auto newSelection = canvasToCellPos(eventButton.x, eventButton.y);
                        _selection.root = newSelection;
                        _selection.end = newSelection;
                        selecting = true;
                        redraw;
                    }

                    if (eventButton.button == 3 && valueSetterMenu !is null)
                    {
                        if (!hasSelection)
                        {
                            ensureSelectionExists;
                            const auto newSelection = canvasToCellPos(eventButton.x, eventButton.y);
                            _selection.root = newSelection;
                            _selection.end = newSelection;
                        }
                        valueSetterMenu.popup(eventButton.button, eventButton.time);
                    }
                    return false;
                });

                addOnMotionNotify((GdkEventMotion* eventMotion, Widget wg) {
                    if (selecting)
                    {
                        ensureSelectionExists;
                        _selection.end = canvasToCellPos(eventMotion.x, eventMotion.y);
                        redraw;
                    }
                    return selecting;
                });

                addOnButtonRelease((GdkEventButton* eventButton, Widget wg) {
                    selecting = false;
                    return false;
                });
            }

            addEvents(EventMask.FOCUS_CHANGE_MASK | EventMask.LEAVE_NOTIFY_MASK);
            addOnFocusOut((GdkEventFocus* e, Widget w) {
                cast(void) w;
                cast(void) e;
                debug writeln("Focus out!");
                if (hasSelection)
                {
                    _selection = null;
                    redraw;
                }
                return false;
            });
        }

        double cellWidth;
        double cellHeight;

        Selection!size_t _selection;
        bool selecting = false;
        bool _editable = true;

        void updateSizeRequest()
        {
            setSizeRequest(cast(int)(cellSize * numberOfColumns), cast(int)(cellSize * numberOfRows));
        }

        @property auto numberOfRows() const
        {
            return gridCollection.height;
        }

        @property auto numberOfColumns() const
        {
            return gridCollection.width;
        }
    }

    public
    {
        /**
          * Constructor that takes a collection. The width and height in cells of the widget is determined from the
          * width and height primitives the collection must provide.
          * The EditingPolicy parameter can be passed to determine the initial editability of the grid.
          * Please note that this only determines whether the collection can be edited through this particular
          * widget via the right click value selector menu. The content of the collection behind it may still be
          * edited from other places. In these cases, make sure to call redraw on the CellGrid object!
          */
        this(CollectionOfE coll, const EditingPolicy editingPolicy = EditingPolicy.Readonly)
        {
            collectionBehind = coll;
            platformSpecificInit;
            _editable = (editingPolicy == EditingPolicy.Editable);
            addEditingEventHandlers;
        }

        private void ensureSelectionExists()
        {
            if (_selection is null)
            {
                _selection = new Selection!size_t();
            }
        }

        /**
          * Convenience function for setting the selection root.
          */
        @property ref auto selectionRoot(const Tuple!(size_t, size_t) newRoot)
        {
            ensureSelectionExists();
            _selection.root = newRoot;
            return this;
        }

        /**
          * Convenience function for setting the selection end.
          */
        @property ref auto selectionEnd(const Tuple!(size_t, size_t) newEnd)
        {
            ensureSelectionExists();
            _selection.end = newEnd;
            return this;
        }

        /**
          * Returns whether the current grid has an active selection.
          */
        @property const auto hasSelection()
        {
            return _selection !is null;
        }

        /**
          * Set whether this CellGrid should have a selection.
          * Setting it to true creates a default selection selecting the upper leftmost cell.
          * Setting it to false clears the current selection.
          */
        @property ref auto hasSelection(bool shouldHaveSelection)
        {
            shouldHaveSelection ? ensureSelectionExists : (_selection = null);
            return this;
        }

        /**
          * Returns whether the collection is directly editable through this widget.
          * Please note that the collection behind the CellGrid might still be edited somewhere else.
          */
        @property const auto editable()
        {
            return _editable;
        }

        /**
          * Sets the editability of the collection directly through this widget.
          * Please note that the collection behind the CellGrid might still be edited somewhere else.
          */
        @property ref auto editable(bool newEditable)
        {
            _editable = newEditable;
            return this;
        }

        /**
          * Convenience function that returns the width in elements of the collection behind the CellGrid.
          */
        @property auto width() const
        {
            return gridCollection.width;
        }

        /**
          * Convenience function to resize the width of the collection behind the CellGrid.
          * Any active selection in the CellGrid widget is cleared.
          */
        @property ref auto width(size_t newWidth)
        {
            gridCollection.width = newWidth;
            if (hasSelection)
            {
                _selection = null;
            }
            updateSizeRequest;
            return this;
        }

        /**
          * Convenience function that returns the height in elements of the collection behind the CellGrid.
          */
        @property auto height() const
        {
            return gridCollection.height;
        }

        /**
          * Convenience function to resize the height of the collection behind the CellGrid.
          * Any Active selection in the CellGrid widget is cleared.
          */
        @property ref auto height(size_t newHeight)
        {
            gridCollection.height = newHeight;
            if (hasSelection)
            {
                _selection = null;
            }
            updateSizeRequest;
            return this;
        }

        /**
          * Set the width and height in pixels of the cell displayed to represent one element.
          * Causes a resize of the widget.
          */
        ref auto setCellSize(const int newCellWidth, const int newCellHeight)
        {
            setSizeRequest(cast(int)(newCellWidth * numberOfColumns),
                    cast(int)(newCellHeight * numberOfRows));
            return this;
        }

        @property ref auto cellSize(const int newSize)
        {
            _cellSize = newSize;
            updateSizeRequest;
            return this;
        }

        /**
          * The width and height in pixels of the cell displayed to represent one element.
          */
        inout @property int cellSize()
        {
            return _cellSize;
        }

        @property ref auto fixedSelectionSize(const size_t width, const size_t height)
        {
            ensureSelectionExists;
            _selection.setFixedSize(width, height);
            return this;
        }

        /**
          * Swaps out the current collection behind this widget for a new one.
          */
        @property ref auto collectionBehind(CollectionOfE newColl)
        {
            assert(newColl.width > 0 && newColl.height > 0, "New grid collection is empty!");
            gridCollection = newColl;
            updateSizeRequest;
            redraw;
            return this;
        }

        /**
          * Convenience access to the collection behind this widget.
          */
        @property auto collectionBehind() inout
        {
            return gridCollection;
        }

        /**
          * Returns the range slice represented by the selection over this CellGrid widget.
          * This is the most convenient way to work with only the selected part of the range.
          */
        inout @property auto selectionRange()
        in
        {
            assert(hasSelection);
        }
        body
        {
            return gridCollection[_selection.top .. _selection.bottom + 1u,
                _selection.left .. _selection.right + 1u];
        }

        private Tuple!(size_t, size_t) canvasToCellPos(double x, double y) inout
        {
            import std.algorithm.comparison : max;

            return tuple(cast(size_t)(max(y, 0.0) / cellHeight),
                    cast(size_t)(max(x, 0.0) / cellWidth));
        }

        version (dlangui)
        {
            override void drawCell(DrawBuf buf, Rect rc, int col, int row)
            {
                const uint colorToDraw = gridCollection[col, row].getDrawColor;
                buf.fillRect(Rect(0, 0, buf.width, buf.height), 0);
            }

            override void drawCellBackground(DrawBuf buf, Rect rc, int col, int row)
            {
                const uint colorToDraw = gridCollection[col, row].getDrawColor;
                buf.fillRect(Rect(0, 0, buf.width, buf.height), 0);
            }
        }

        version (gtkd)
        {
            override void draw(Context cairoContext)
            {
                import std.range;

                cellWidth = getAllocatedWidth() / numberOfColumns;
                cellHeight = getAllocatedHeight() / numberOfRows;

                foreach (const rowIndex; iota(0, numberOfRows))
                {
                    foreach (const columnIndex; iota(0, numberOfColumns))
                    {
                        const auto currentColor = gridCollection[rowIndex, columnIndex]
                            .getDrawColor;
                        cairoContext.setSourceRgb(currentColor.r, currentColor.g, currentColor.b);
                        cairoContext.setLineWidth(1);
                        cairoContext.rectangle(cellWidth * columnIndex,
                                cellHeight * rowIndex, cellWidth, cellHeight);
                        cairoContext.strokePreserve;
                        cairoContext.fill;
                    }
                }

                if (_editable && hasSelection)
                {
                    with (cairoContext)
                    {
                        setSourceRgb(0.0, 0.2, 0.7);
                        rectangle(cellWidth * _selection.left, cellHeight * _selection.top,
                                cellWidth * (_selection.width + 1u),
                                cellHeight * (_selection.height + 1u));
                        setLineWidth(2);
                        strokePreserve;
                        setSourceRgba(0.0, 0.2, 0.7, 0.2);
                        fill;
                    }
                }
            }

            /**
              * Force redraw the contents of this CellGrid. Use when the contents of the collection behind this widget
              * were modified outside of the widget.
              */
            void redraw()
            {
                queueDrawArea(0, 0, getAllocatedWidth(), getAllocatedHeight());
            }

            /**
              * Convenience function to automatically add the names and values of an enum to the value setter menu.
              */
            ref auto addEnumValuesToValueSetterMenu(EnumType)()
            {
                import capsicum.utils.Enum : NameValuePairs;
                import std.array : byPair;

                return addNamedValuesToValueSetterMenu(NameValuePairs!EnumType);
            }

            /**
              * Add a set of named preset values to the value setter menu.
              */
            ref auto addNamedValuesToValueSetterMenu(PairRange)(PairRange valuePairs)
            {
                if (valueSetterMenu is null)
                {
                    valueSetterMenu = new Menu();
                }

                foreach (name, value; valuePairs)
                {
                    import std.conv : to;

                    valueSetterMenu.append(new MenuItem((MenuItem m) {
                            foreach (index; selectionRange.IndicesOf)
                            {
                                selectionRange[index] = value;
                            }
                            redraw;
                        }, name));
                }
                valueSetterMenu.showAll;
                return this;
            }

            @property pure auto maxWidth() const
            {
                return gridCollection.maxWidth;
            }

            @property pure auto maxHeight() const
            {
                return gridCollection.maxHeight;
            }
        }
    }
}
