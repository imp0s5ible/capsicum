module capsicum.adapters.ProjectWindow;

debug import std.stdio : write, writeln;

debug import std.format : format;

import capsicum.Project;
import capsicum.adapters.MapEditor;
import capsicum.adapters.RuleList;
import capsicum.adapters.MatchControl;

version (gtkd)
{
    import gtk.Widget;
    import gdk.Event;
    import gtk.MainWindow;
    import gtk.MenuBar;
    import gtk.Grid;
    import gtk.Button;
    import gtk.Label;
    import gtk.Frame;

    class ProjectWindow : MainWindow
    {
        package
        {
            Project project;
            MapEditor!(Project.MapType) mapEditor;
            Grid layoutGrid;
            MatchControl matchControl;

            MenuBar menuBar;

            RuleList!(Project.MapType) ruleListWidget;
        }

        this(Project project)
        {
            this.project = project;
            super("Capsicum Pattern Matcher - " ~ this.project.name);
            initialize;
        }

        private void initialize() {
            setupMapEditor;
            setupMatchControl;
            setupRuleListWidget;
            setupMenuBar;
            setupLayout;
            setupEventHandlers;
            showAll;
        }

        private void setupMapEditor()
        {
            mapEditor = new MapEditor!(typeof(project.map))(project.map);
            mapEditor.addEnumValuesToValueList!NamedCellType;
        }

        private void setupMatchControl() {
            matchControl = new MatchControl(this);
        }

        private void setupRuleListWidget()
        {
            ruleListWidget = createRuleList!(typeof(project.map))(mapEditor, project.rules);
            ruleListWidget.ruleGenerator = {
                import capsicum.patternMatching.RuleSetting : createRuleSetting;

                return project.generateRandomRule;
            };
            ruleListWidget.addEnumValuesToValueSetterMenu!NamedCellType;
        }

        private void setupMenuBar()
        {
            import gtk.Menu : Menu;
            import gtk.MenuItem : MenuItem;

            menuBar = new MenuBar();
            auto fileMenuItem = new MenuItem("File");
            auto fileMenu = new Menu();
            auto saveMenuItem = new MenuItem("Save Project");
            saveMenuItem.addOnButtonRelease((Event e, Widget w) {
                cast(void) e;
                cast(void) w;
                openProjectSaveDialog;
                return false;
            });
            auto loadMenuItem = new MenuItem("Load Project");
            loadMenuItem.addOnButtonRelease((Event e, Widget w) {
                cast(void) e;
                cast(void) w;
                openProjectLoadDialog;
                initialize;
                return false;
            });
            with (fileMenu)
            {
                append(saveMenuItem);
                append(loadMenuItem);
            }
            fileMenuItem.setSubmenu(fileMenu);
            menuBar.append(fileMenuItem);
        }

        private void setupLayout()
        {
            import gtk.Box : Box, Orientation;

            removeAll;
            layoutGrid = new Grid();
            with (layoutGrid)
            {
                setMarginTop(10u);
                setMarginBottom(10u);
                setMarginLeft(10u);
                setMarginRight(10u);
                setColumnSpacing(10u);
                setRowSpacing(10u);
                attach(new Frame(mapEditor, "Map Editor"), 0, 0, 3, 1);
                attach(new Frame(matchControl, "Match Control"), 0, 1, 1, 1);
                attach(new Frame(ruleListWidget, "Rule List"), 3, 0, 1, 5);
            }
            auto box = new Box(Orientation.VERTICAL, 10);
            box.packStart(menuBar, false, false, 0);
            box.packStart(layoutGrid, false, false, 0);
            setDefaultSize(200, 100);
            setResizable(false);
            add(box);
        }

        private void setupEventHandlers()
        {
        }

        ~this()
        {
        }

        import std.typecons : Tuple;

        private Tuple!(int, "response", string, "filename") getFileNameFromDialog(string title,
                FileChooserAction action)
        {
            import gtk.FileChooserDialog;

            auto saveDialog = new FileChooserDialog(title, this, action,
                    ["Choose Folder", "Cancel"]);
            auto response = saveDialog.run;
            saveDialog.close;
            return Tuple!(int, "response", string, "filename")(response, saveDialog.getFilename());
        }

        private void openProjectSaveDialog()
        {
            import std.algorithm : map;

            auto projectSaveData = getFileNameFromDialog("Save Project",
                    FileChooserAction.SELECT_FOLDER);
            if (projectSaveData.response == GtkResponseType.OK)
            {
                project.saveToDirectory(projectSaveData.filename);
            }
        }

        private void openProjectLoadDialog()
        {
            auto projectLoadData = getFileNameFromDialog("Load Project",
                    FileChooserAction.SELECT_FOLDER);
            if (projectLoadData.response == GtkResponseType.OK)
            {
                project.loadFromDirectory(projectLoadData.filename);
            }
        }

    }
}
